# FlyZone #

## What is it ##

FlyZone provides developers with a high-level interface designed to smooth the transition from testbed to real deployments, is portable across drone platforms, allows one to customize the testbed installation depending on the target site, and includes a dedicated [indoor localization](https://bitbucket.org/neslabpolimi/localisation_camera) technique we expressly designed for testbed operation.

![](https://bitbucket.org/neslabpolimi/infrastructure/wiki/img/arch.pdf)

The drone control component orchestrates the operation of the testbed. It receives as input the location of every drone and sends commands back to the drones to steer their movements according to the application logic. The component exists in only one copy and is deployed on a sufficiently powerful machine we call control station. The component steers every deployed drone through a platform-specific API, which is typically provided by the drone manufacturer or based on existing standards.

The *low-level localization* component collects the raw data necessary to localize a drone. For example, it gathers signal strength information if localization employs radio technology, or it acquires raw video frames if localization uses visual techniques, as we do. There may be multiple such components deployed for a single drone. The *low-level localization* component is necessarily deployed aboard the drone. However, this component does not necessarily need to connect to the drone software stack. For example, our [visual localisation technique](https://bitbucket.org/neslabpolimi/localisation_camera) requires no such integration. As such, it can be ported to different drones by simply physically installing the necessary hardware, yet with no hardware/software integration needed. This facilitates porting FlyZone to different drone platforms, as long as they can physically carry the necessary hardware.
The *high-level localization* component takes as input the data from multiple *low-level localization* components and performs the processing needed to output the position of the drone. There are, as a result, as many of these components as the number of drones deployed in the testbed. Decoupling the raw data acquisition from the higher-level processing allows one to obtain a more flexible implementation of localization techniques that combine diverse inputs.

An additional benefit of decoupling the *low-level* and *high-level* localization components is the ability to deploy the latter independent of the former. Normally, the *high-level localization* component would be deployed on the control station. Differently, if the hardware aboard a drone is sufficiently powerful, the *low-level* and *high-level* localization components may be co-located on the drone. This provides the drone with localization information derived locally. Further, such a configuration may help lower the processing load on the control station, helping scalability.

## OK I buy it! ##

* Make sure you went through the [Quick Setup Guide](https://bitbucket.org/neslabpolimi/infrastructure/wiki/misc/guide.pdf).

* Then Follow the [Network Recommendations](https://bitbucket.org/neslabpolimi/infrastructure/wiki/network).

* We also prepared a guide for [AR.Drone 2.0 deployment](https://bitbucket.org/neslabpolimi/infrastructure/wiki/ardrone2).

* After yo set up everything, you may want to checkout how to [Pilot](https://bitbucket.org/neslabpolimi/infrastructure/wiki/scenario) drones.

* [Architecture](https://bitbucket.org/neslabpolimi/infrastructure/wiki/architecture) and [Navigation Strategy](https://bitbucket.org/neslabpolimi/infrastructure/wiki/ardrone2navstrategy) will give you more detailed info on what's going on in the software.