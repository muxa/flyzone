# FlyZone localisation #

This project is dedicated to a visual-based [UAV's navigation](https://bitbucket.org/neslabpolimi/infrastructure).

The project supports two localisation approaches:

 * *Decentralised* when the recognition occurs on the RaspberryPi. In this case, the latter broadcasts decoded coordinates to the server that runs a control loop.

 * *Centralised* when recognition occurs on the stationary PC. In this case, every RaspberryPi broadcasts a video stream to the stationary PC that runs recognition algorithm and broadcasts the decoded coordinates.

### Compilation and launch ###

#### Ubuntu ####

make sure you've ```java, g++, ant, OpenCV``` installed

clone sources:
```
$ git clone https://bitbucket.org/neslabpolimi/localisation_camera
```

build the detector with ant:
```
$ cd localisation_camera
$ ant jar_lin
```

then run it:
```
$ ant run_lin
```

If the software is run under Ubuntu OS, it will automatically use *centralised* approach.

#### RaspberryPi ####

this guide targets the freshly installed [NOOBS](https://www.raspberrypi.org/downloads/noobs/).

make sure you have java 7 and g++ installed (they're usually delivered with NOOBS OS)
```
$ java -version
$ g++ --version
```

install ant:
```
$ sudo apt-get install ant
```

install cmake
```
$ sudo apt-get install cmake
```

install gstreamer for centralized approach
```
$ sudo apt-get install gstreamer-tools gstreamer1.0-*
```

install OpenCV 2.4.9:
```
$ git clone https://github.com/opencv/opencv.git
$ cd opencv
$ git checkout 2.4.9.1
$ mkdir build
$ cd build
$ cmake -D CMAKE_BUILD_TYPE=RELEASE -D WITH_FFMPEG=OFF ../
$ make
$ sudo make install
```

enable raspicam:
```
$ sudo raspi-config
```
* choose "Enable Camera" option

clone sources:
```
$ git clone https://bitbucket.org/neslabpolimi/localisation_camera
```

build the detector with ant:
```
$ cd localisation_camera
$ ant jar_rpi
```

then run it in *decentralised* mode:
```
$ ant run_rpi
```

or in *centralised* mode:
```
$ ant stream
```

#### General Notes ####

There are two recognition cores: ```AprilTags``` and ```ColorTags```. The latter is the default. To enable the former, just modify the configuration file and make sure you're using the correct tags. The generated tags can be found in the Downloads section.

You may want to checkout the [Architecture](https://bitbucket.org/neslabpolimi/localisation_camera/wiki/architecture) and [Network](https://bitbucket.org/neslabpolimi/infrastructure/wiki/network) to get more info on how is the software functioning.