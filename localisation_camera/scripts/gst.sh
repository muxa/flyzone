export GST_DEBUG_FILE=$PWD/$5.log
/opt/vc/bin/raspivid -t 0 -mm matrix -rot 270 -awb tungsten -fps 20 -w $1 -h $2 -o - | gst-launch-1.0 fdsrc ! h264parse ! rtph264pay config-interval=1 pt=96 ! udpsink host=$3 port=$4 --gst-debug=h264parse:9
