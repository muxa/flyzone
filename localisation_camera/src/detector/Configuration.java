package detector;

import java.awt.Dimension;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Configuration {
	
	private static final Logger log = Logger.getLogger( Configuration.class.getName() );
	
	// recognition cores
	public static final String APRILTAGS = "apriltag";
	public static final String COLORTAGS = "colortag";
	
	// resolutions
	public static final Dimension R240P = new Dimension(320, 240);
	public static final Dimension R480P = new Dimension(640, 480);
	public static final Dimension R768P = new Dimension(1024, 768);
	public static final Dimension R1080P = new Dimension(1920, 1080);
	
	// cmd key words
	private static final String FILE_KEY = "--file";
	
	// config key words
	private static final String METHOD_KEY = "method";
	private static final String RESOLUTION_KEY = "resolution";
	private static final String BROADCASTIP_KEY = "broadcastIP";
	private static final String STREAMCONTROLPORT_KEY = "streamControlPort";
	private static final String LOCALISATIONPORT_KEY = "localisationPort";
	private static final String VISUALIZE_KEY = "visualize";
	private static final String MEASURELATENCY_KEY = "measureLatency";
	private static final String R240P_KEY = "240p";
	private static final String R480P_KEY = "480p";
	private static final String R768P_KEY = "768p";
	private static final String R1080P_KEY = "1080p";
	
	private static final String CONFIGURATION = System.getProperty("user.dir")+"/detector.conf";
	
	private static String METHOD = COLORTAGS;
	private static Dimension RESOLUTION = R480P;
	private static boolean VISUALIZE = false;
	protected static boolean MEASURELATENCY = false;
	// network
	private static String BROADCASTIP = "192.168.1.255";
	private static int LOCALISATIONPORT = 2016;
	private static int STREAMCONTROLPORT = 5000;
	
	private static HashMap<String, String> _args = new HashMap<String, String>();
	
	public static void init(String[] args) throws IOException {
		parseArgs(args);
		initFromConfig(getConfigurationPath());
	}
	
	public static HashMap<String, String> parseArgs(String[] args) {
		for (int i=0; i<args.length;i+=2) {
			if (i+1 >= args.length) break;
			switch(args[i]){
			case FILE_KEY:
				_args.put(FILE_KEY, args[i+1]);
				break;
			default:
				log.info("Unknown argument "+args[i]+"! Allowed arguments are:\n"+
						 FILE_KEY);
				break;
			}
		}
		return _args;
	}
	
	public static void initFromConfig(String path) throws IOException{
		String configuration = path == null || path.isEmpty() ? CONFIGURATION
											  				  : path;
		for (String line: Files.readAllLines(Paths.get(configuration), Charset.defaultCharset())) {
			if (line.startsWith("#") || line.isEmpty()) continue;
			String parameter = line.split("=")[0];
			String value = line.split("=")[1];
			set(parameter, value);
		}
	}
	
	public static void set(String parameter, String value) {
		switch(parameter){
		case METHOD_KEY:
			setMethod(value);
			break;
		case RESOLUTION_KEY:
			setResolution(value);
			break;
		case BROADCASTIP_KEY:
			BROADCASTIP = value;
			break;
		case LOCALISATIONPORT_KEY:
			LOCALISATIONPORT = Integer.parseInt(value);
			break;
		case STREAMCONTROLPORT_KEY:
			STREAMCONTROLPORT = Integer.parseInt(value);
			break;
		case VISUALIZE_KEY:
			VISUALIZE = Boolean.parseBoolean(value);
			break;
		case MEASURELATENCY_KEY:
			MEASURELATENCY = Boolean.parseBoolean(value);
			break;
		default:
			log.info("Unknown parameter "+parameter+"! Allowed parameters are:\n"+
					 METHOD_KEY+"\n"+
					 RESOLUTION_KEY+"\n"+
					 BROADCASTIP_KEY+"\n"+
					 LOCALISATIONPORT_KEY+"\n"+
					 STREAMCONTROLPORT_KEY+"\n"+
					 MEASURELATENCY_KEY);
			break;
		}
	}
	
	public static void setResolution(String resolution_key) {
		switch(resolution_key) {
		case R240P_KEY:
			RESOLUTION = R240P;
			break;
		case R480P_KEY:
			RESOLUTION = R480P;
			break;
		case R768P_KEY:
			RESOLUTION = R768P;
			break;
		case R1080P_KEY:
			RESOLUTION = R1080P;
			break;
		default:
			log.info("Unknown resolution "+resolution_key+"! Allowed resolutions are:\n"+
					 R240P_KEY+"\n"+
					 R480P_KEY+"\n"+
					 R768P_KEY+"\n"+
					 R1080P_KEY+"\n");
			break;
		}
	}

	public static void setMethod(String method) {
		switch (method) {
		case APRILTAGS:
			log.log(Level.INFO, "------------[Using AprilTags detector]------------");
			METHOD = APRILTAGS;
			break;
		case COLORTAGS:
			log.log(Level.INFO, "------------[Using ColorTags detector]------------");
			METHOD = COLORTAGS;
			break;
		default:
			log.info("Unknown method "+method+"! Allowed methods are:\n"+
					 APRILTAGS+"\n"+
					 COLORTAGS);
			break;
		}
	}
	
	public static String get(String parameter) {
		if (!_args.containsKey(parameter)) {
			log.info("No such parameter "+parameter+"! List of parameters:");
			for(String key:_args.keySet()) log.info(key);
			return "";
		}
		return _args.get(parameter);
	}
	
	public static String getConfigurationPath() {
		return get(FILE_KEY);
	}
	
	public static String getMethod() {
		return METHOD;
	}
	
	public static Dimension getResolution() {
		return RESOLUTION;
	}
	
	public static int getWidth() {
		return RESOLUTION.width;
	}
	
	public static int getHeight() {
		return RESOLUTION.height;
	}
	
	public static String getBroadcastIp() {
		return BROADCASTIP;
	}
	
	public static int getLocalisationPort() {
		return LOCALISATIONPORT;
	}
	
	public static int getStreamControlPort() {
		return STREAMCONTROLPORT;
	}
	
	public static boolean getVisualize() {
		return VISUALIZE;
	}
	
	public static boolean getMeasureLatency() {
		return MEASURELATENCY;
	}
	
}
