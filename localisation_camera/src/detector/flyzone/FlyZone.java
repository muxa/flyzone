package detector.flyzone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import detector.Configuration;
import detector.Platform;
import detector.Utils;
import detector.aprilzone.AprilZone;

public class FlyZone {
	
	private String _solution;
	HashMap<String, Point> _COORDS = new HashMap<String, Point>();
	@SuppressWarnings("rawtypes") HashMap[] _COLORS = new HashMap[6];
	 public double[] _coords = new double[3];
	ExecutorService _executor;
	public int _threads = 0;

	public FlyZone (String solution, int threads) {
		// picam colors
		HashMap<String, Scalar> yellow = new HashMap<String, Scalar>();
		yellow.put("lower", new Scalar(20, 50, 50));
		yellow.put("upper", new Scalar(38, 255, 255));
		HashMap<String, Scalar> green = new HashMap<String, Scalar>();
		green.put("lower", new Scalar(43, 30, 30));
		green.put("upper", new Scalar(70, 255, 255));
		HashMap<String, Scalar> cyan = new HashMap<String, Scalar>();
		cyan.put("lower", new Scalar(74, 50, 50));
		cyan.put("upper", new Scalar(85, 255, 255));
		HashMap<String, Scalar> blue = new HashMap<String, Scalar>();
		blue.put("lower", new Scalar(86, 50, 50));
		blue.put("upper", new Scalar(100, 255, 255));
		HashMap<String, Scalar> magenta = new HashMap<String, Scalar>();
		magenta.put("lower", new Scalar(150, 50, 50));
		magenta.put("upper", new Scalar(179, 255, 255));
		//HashMap<String, Scalar> red = new HashMap<String, Scalar>();
		//red.put("lower", new Scalar(171, 50, 50));
		//red.put("upper", new Scalar(179, 255, 255));
		HashMap<String, Scalar> red = new HashMap<String, Scalar>();
		red.put("lower", new Scalar(0, 50, 50));
		red.put("upper", new Scalar(19, 255, 255));
		
		_COLORS[0] = yellow;
		_COLORS[1] = green;
		_COLORS[2] = cyan;
		_COLORS[3] = blue;
		_COLORS[4] = magenta;
		_COLORS[5] = red;
		//_COLORS[6] = red_2;
	    _solution = solution;
	    _executor = Executors.newFixedThreadPool(threads);
	    _threads = threads;
	}

	public void initialize() {
		// reading the solution
		FileReader fileReader = null;
		Pattern regex = Pattern.compile("\\d+");
		try {
			fileReader = new FileReader(_solution);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader bufferedReader =  new BufferedReader(fileReader);
		String line;
		try {
			for (int i = 0; i < 23; i++) {
				for (int j = 0; j < 23; j++) {
					line = bufferedReader.readLine();
					Matcher m = regex.matcher(line);
					m.find();
					int r = Integer.parseInt(m.group());
					m.find();
					int c = Integer.parseInt(m.group());
					m.find();
					int v = Integer.parseInt(m.group());
					if (r != i || c != j) {
						System.out.println("Error! r="+r+" c="+c+", but must be "+i+" "+j);
						return;
					}
					// decode value into colors' numbers
					String value = "";
					for (int k = 3; k >= 0; k--) {
						value += " "+(int)(v/Math.pow(6, k) % 6);
					}
					_COORDS.put(value, new Point(i, j));
				}
			}
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public double[] process_parallel(Mat mat, int max_tags, final double[] target, double area_threshold, double gray_threshold, boolean multithread){
		// convert image BGR (OpenCV native)
		//Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2BGR);
		//mat.convertTo(mat, CvType.CV_8UC3, 2, -100);
		
		// grayscaling
		//System.out.println(mat.type()+" "+CvType.CV_8UC3+" "+CvType.CV_8U);
		Mat gray = new Mat(mat.rows(),mat.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY);
        
        // threshold image
        Mat thresh = new Mat(gray.rows(), gray.cols(), gray.type());
        Imgproc.threshold(gray, thresh, gray_threshold, 255 , Imgproc.THRESH_BINARY_INV);
        
        // finding contours
        Mat hierarchy = new Mat();
        List<MatOfPoint> points = new ArrayList<>();
        Imgproc.findContours(thresh, points, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        
        //Imgproc.drawContours(mat, points, 0, new Scalar(0, 255, 0), 3);
        
        // create list for each of workers
        List< List<MatOfPoint> > lst_points = new ArrayList< List<MatOfPoint> >();
        for (int i = 0; i < _threads; i++) lst_points.add(new ArrayList<MatOfPoint>());
        
        // splitting contours into lists
        for (int i = 0; i < points.size(); i++) lst_points.get(i % _threads).add(points.get(i));
        
        List<RotatedRect> squares = filter_squares(lst_points, area_threshold, multithread);
        
        
        // rebalancing and get min area
        double min_area = 0;
        List< List<RotatedRect> > lst_squares = new ArrayList< List<RotatedRect> >();
        for(int i = 0; i < _threads; i++) lst_squares.add(new ArrayList<RotatedRect>());
        for(int i = 0; i < squares.size(); i++) {
        	RotatedRect square = squares.get(i);
        	if (min_area == 0 || min_area > square.size.area()) min_area = square.size.area();
        	lst_squares.get(i % _threads).add(square);
        }
        
        List<Tag> tags = recognize_tags(mat, lst_squares, squares, max_tags/_threads, min_area, multithread);
        
        //System.out.println("Tags: "+tags.size());
        
        //List< MatOfPoint> cnts = new ArrayList<MatOfPoint>();
        //cnts.add(new MatOfPoint(min_cnt.toArray()));
        //Imgproc.drawContours(mat, cnts, 0, new Scalar(0, 255, 0), 3);
        
        // rebalancing and get mean_area and mean_angle
        double mean_area = 0;
    	double mean_angle = 0;
        List<List<Tag> > lst_tags = new ArrayList<List<Tag> >();
        for (int i = 0; i < _threads; i++) lst_tags.add(new ArrayList<Tag>());
        for (int i = 0; i < tags.size(); i++) {
        	Tag tag = tags.get(i);
        	mean_area += tag._rect.size.area();
            mean_angle += tag.get_angle();
            lst_tags.get(i % _threads).add(tag);
        }
        if (tags.size() > 0) {
            mean_angle = mean_angle/tags.size();
            mean_area = mean_area/tags.size();
    	}
        
        estimate(mat.rows(),mat.cols(), target, mean_area, lst_tags, multithread);
    	
    	if (_coords != null) _coords[2] = mean_angle;
    	
    	//for (Tag tag : tags) tag.draw_on(mat);
        
        return _coords;
	}
	
	public double[] process(Mat mat, int max_tags, final double[] target, double area_threshold, boolean multithread){
		// convert image BGR (OpenCV native)
		//Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2BGR);
		//mat.convertTo(mat, CvType.CV_8UC3, 2, -100);
		
		// grayscaling
		Mat gray = new Mat(mat.rows(),mat.cols(), CvType.CV_8UC1);
        Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY);
        
        // threshold image
        Mat thresh = new Mat(gray.rows(), gray.cols(), gray.type());
        Imgproc.threshold(gray, thresh, 155 , 255 , Imgproc.THRESH_BINARY_INV);
        
        // finding contours
        Mat hierarchy = new Mat();
        List<MatOfPoint> points = new ArrayList<>();
        Imgproc.findContours(thresh, points, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        
        // minimum area and center of that area
        double min_area = 0;
        MatOfPoint min_cnt = null;
        List<RotatedRect> meaningful_squares = new ArrayList<RotatedRect>();
        
        for (MatOfPoint cnt : points) {
        	RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
        	//double area = Imgproc.contourArea(cnt);
        	if (rrect.size.area() < area_threshold) continue;
        	//RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
        	if (Utils.almost_rectangular(rrect)) {
        		
        		if (min_cnt == null || min_area > rrect.size.area()) {
        			/*
        			Point[] rpts = new Point[4];
            		rrect.points(rpts);
            		// skip the contour sticked to the borders
                	// as we need nice and solid rectangles from the center
                	boolean skip = false;
                	for (int i = 0; i < rpts.length; i++)
                		if (rpts[i].x <= 10 || rpts[i].y <= 10 ||
                			rpts[i].x >= mat.cols()-10 || rpts[i].y >= mat.rows()-10) {
                			skip = true;
                			break;
                		}
                	
        			if (!skip) {*/
        				min_area = rrect.size.area();
        				min_cnt = cnt;
        			//}
        		}
        		meaningful_squares.add(rrect);
        	}
        }
        
        //List< MatOfPoint> cnts = new ArrayList<MatOfPoint>();
        //cnts.add(new MatOfPoint(min_cnt.toArray()));
        //Imgproc.drawContours(mat, cnts, 0, new Scalar(0, 255, 0), 3);
        
        List<List<Tag> > tags = new ArrayList<List<Tag> >();
        for (int i = 0; i < _threads; i++) {
        	List<Tag> tags_i = new ArrayList<Tag>();
        	tags.add(tags_i);
        }
        int count = 0;

    	double mean_area = 0;
    	double mean_angle = 0;
        for (RotatedRect rect : meaningful_squares) {
        	if (rect == null) continue;
        	// here we need to check only big boxes
        	if (!Utils.almost(rect.size.area(), min_area*4)) continue;
        	
        	//draw(mat, rect);
        	
        	for (RotatedRect other_rect : meaningful_squares) {
        		if (other_rect == null) continue;
        		// here we need to check only small boxes
        		if (!Utils.almost(other_rect.size.area(), min_area)) continue;
        		
        		//draw(mat, other_rect);
        		
        		Point[] rpts = new Point[4];
        		rect.points(rpts);
        		//System.out.println("in box:"+Utils.in_box(other_rect.center, rpts));
        		if (!Utils.in_box(other_rect.center, rpts)) continue;
        		//System.out.println("full:"+Utils.full(mat, rect));
        		if (!Utils.full(mat, rect)) continue;
        		
        		Tag tag = new Tag(this, mat, rect, other_rect.center);
        		tag.init();
        		//System.out.println(tag.get_value());
	            mean_area += rect.size.area();
	            mean_angle += tag.get_angle();
	            tags.get(count % _threads).add(tag);
	            count++;
	            //is_tag = true;
	            break;
        	}
        	
            if (count > max_tags) break;
        }
        //System.out.println(count+" "+area_threshold);
        if (count > 0) {
            mean_angle = mean_angle/count;
            mean_area = mean_area/count;
    	}
        
        estimate(mat.rows(),mat.cols(), target, mean_area, tags, multithread);
    	
    	if (_coords != null) _coords[2] = mean_angle;
    	
    	//if((Platform.Linux || Platform.MACOS) && Configuration.getVisualize())
    		for (List<Tag> list : tags) for (Tag tag : list) tag.draw_on(mat);
        
        return _coords;
	}
	
	private List<Tag> recognize_tags(Mat frame, List< List<RotatedRect> > lst_squares, List<RotatedRect> all_squares, int max_tags, double min_area, boolean multithread) {
		List<TagRecognizer> recognizers = new ArrayList<TagRecognizer>();
        for (List<RotatedRect> squares : lst_squares) {
        	//System.out.println(list.size());
        	recognizers.add(new TagRecognizer(this, frame, squares, all_squares, max_tags, min_area));
        }
        List<Tag> tags = new ArrayList<Tag>();
        if (multithread) {
        	List<Future<List<Tag>>> results = null;
        	try {
        		results = _executor.invokeAll(recognizers);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        	for (Future< List<Tag> > result : results)
        		if (result.isDone()) 
        			try {
        				tags.addAll(result.get());
        			} catch (InterruptedException | ExecutionException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        } else {
        	for (TagRecognizer recognizer : recognizers)
        		try {
    				tags.addAll(recognizer.call());
    			} catch (InterruptedException | ExecutionException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
        
    	return tags;
	}

	private List<RotatedRect> filter_squares(List< List<MatOfPoint> > lst_points, double thresh, boolean multithread) {
		List<Filter> filters = new ArrayList<Filter>();
        for (List<MatOfPoint> points : lst_points) {
        	//System.out.println(list.size());
        	filters.add(new Filter(points, thresh));
        }
        List<RotatedRect> squares = new ArrayList<RotatedRect>();
        if (multithread) {
        	List<Future<List<RotatedRect>>> results = null;
        	try {
        		results = _executor.invokeAll(filters);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        	for (Future< List<RotatedRect> > result : results)
        		if (result.isDone()) 
        			try {
        				squares.addAll(result.get());
        			} catch (InterruptedException | ExecutionException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        } else {
        	for (Filter filter : filters)
        		try {
    				squares.addAll(filter.call());
    			} catch (InterruptedException | ExecutionException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
        
    	return squares;
	}

	void draw(Mat mat, RotatedRect rect) {
		Point[] rpts = new Point[4];
		rect.points(rpts);
		List< MatOfPoint> cnts = new ArrayList<MatOfPoint>();
        cnts.add(new MatOfPoint(rpts));
        Imgproc.drawContours(mat, cnts, 0, new Scalar(0, 255, 0), 3);
	}

	public void estimate(int height, int width, double[] target, double mean_area, List< List<Tag> > tags, boolean multithread) {
		//System.out.println(">---------------------------------------------");
        List<Estimator> estimators = new ArrayList<Estimator>();
        for (List<Tag> list : tags) {
        	//System.out.println("Tags for estimator worker "+list.size());
                if (list.size() == 0) continue;
        	estimators.add(new Estimator(new Size(width, height), target, mean_area, list));
        }
        List<Point> c_r = new ArrayList<Point>();
        if (multithread) {
        	List<Future<List<Point>>> results = null;
        	try {
        		results = _executor.invokeAll(estimators);
        	} catch (InterruptedException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
        	for (Future< List<Point> > result : results)
        		if (result.isDone())
        			try {
        				c_r.addAll(result.get());
        			} catch (InterruptedException | ExecutionException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        } else {
        	for (Estimator est : estimators)
        		try {
    				c_r.addAll(est.call());
    			} catch (InterruptedException | ExecutionException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        }
    	
    	List<Point> filtered = new ArrayList<Point>();
    	Point std_dev = Utils.med_std_dev(c_r);
    	Point mean = Utils.median(c_r);
    	//System.out.println("Estimated "+c_r+" points");
    	for (Point pt : c_r) {
    		//System.out.println(pt+" "+std_dev+" "+(pt.x-mean.x)+" "+(pt.y-mean.y));
    		if (this instanceof AprilZone) {
    			if (Math.abs(pt.x-mean.x) > std_dev.x || Math.abs(pt.y-mean.y) > std_dev.y) continue;
    		} else {
    			if (Math.abs(pt.x-mean.x) > std_dev.x/2 || Math.abs(pt.y-mean.y) > std_dev.y/2) continue;
    		}
    		filtered.add(pt);
    	}
    	if (filtered.size() > 0) {
    		Point mean2 = Utils.mean(filtered);
    		_coords = new double[]{mean2.x, mean2.y, 0};
    	} else {
    		_coords = null;
    	}
	}
	
	private void filter(MatOfPoint cnt, double area_thershold, List<RotatedRect> meaningful_squares) {
		RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
		//double area = Imgproc.contourArea(cnt);
		if (rrect.size.area() < area_thershold) return;
		//RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
		if (Utils.almost_rectangular(rrect)) {
			//if (min_cnt == null || min_area > rrect.size.area()) {
			//	min_cnt = cnt;
			//	min_area = rrect.size.area();
			//}
			meaningful_squares.add(rrect);
		}
	}
}
