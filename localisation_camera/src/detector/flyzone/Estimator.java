package detector.flyzone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.opencv.core.Point;
import org.opencv.core.Size;

import detector.Utils;
import detector.aprilzone.AprilTag;

public class Estimator implements Callable< List<Point> >{
	
	private double _w;
	private double _h;
	private double[] _t;
	private List<Tag> _tags;
	private double _a;
	private Class _tagClass;
	
	public Estimator(Size wh, double[] target, double mean_area, List<Tag> tags) {
		_w = wh.width;
		_h = wh.height;
		_t = target;
		_a = mean_area;
		_tags = tags;
	}
	
	@Override
	public List<Point> call() throws Exception {
		// center of the frame
		Point c = new Point(_w*_t[0], _h*_t[1]);
		// approximate #pixels per unit of real coords
		double s = Math.sqrt(_a)*1.12;
		// maximum radius
		double mr;
		if (s > 0 ) mr = Utils.length(_w/s, _h/s, 0, 0)/2;
		else mr = 0;
		List<Point> c_r = new ArrayList<Point>();
		for (Tag tag : _tags) {
			double a = 0;
			if (tag instanceof AprilTag) a = ((AprilTag)tag).get_angle();
			if (tag instanceof Tag) a = ((Tag)tag).get_angle();
			//System.out.println("Estimated angle: "+a);
			
			Point v = null;
			if (tag instanceof AprilTag) v = ((AprilTag)tag).get_value();
			if (tag instanceof Tag) v = ((Tag)tag).get_value();
                        //System.out.println("Estimated value: "+v);
			if (v == null) continue;
			Point t=null;
			if (tag instanceof AprilTag) t = ((AprilTag)tag).get_center();
			if (tag instanceof Tag) t = ((Tag)tag).get_center();
                        //System.out.println("Estimated center: "+t);
			// vector from tag to frame's center in pixels
			Point tc = new Point(c.x-t.x, c.y-t.y);
			//Core.line(mat, t, c, new Scalar(255, 0, 0), 2);
			// rotate coordinates according to the real orientation
			Point tc_r = new Point(tc.x*Math.cos(-a*Math.PI/180)-tc.y*Math.sin(-a*Math.PI/180),
								   tc.x*Math.sin(-a*Math.PI/180)+tc.y*Math.cos(-a*Math.PI/180));
			//Core.line(mat, new Point(0, 0), tc_r, new Scalar(255, 0, 0), 2);
			// calculate vector from tag to center in real coords
			Point vc_r = new Point(tc_r.x/s, tc_r.y/s);
			// filter out incorrectly recognized tags
                        //System.out.println("estimated length "+Utils.length(vc_r)+" limit "+mr);
			//if (Utils.length(vc_r) > mr) continue;
			// calculating real coords of the center and add it to the mean
			c_r.add(new Point(v.x+vc_r.x, v.y+vc_r.y));
			//System.out.println(v+" "+(v.x+vc_r.x)+" "+(v.y+vc_r.y));
			//tag.draw_on(mat);
			//break;
		}
		return c_r;
	}
}
