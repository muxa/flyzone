package detector.flyzone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;

import detector.Utils;

public class TagRecognizer implements Callable< List<Tag> >{
	
	private List<RotatedRect> _part_squares;
	private List<RotatedRect> _all_squares;
	private double _min_area;
	private Mat _mat;
	private FlyZone _fz;
	private int _max_tags;

	public TagRecognizer(FlyZone fz, Mat mat, List<RotatedRect> part_squares, List<RotatedRect> all_squares, int max_tags, double min_area) {
		_part_squares = part_squares;
		_all_squares = all_squares;
		_min_area = min_area;
		_mat = mat;
		_fz = fz;
		_max_tags = max_tags;
	}

	@Override
	public List<Tag> call() throws Exception {
		List<Tag> tags = new ArrayList<Tag>();
		for (RotatedRect rect : _part_squares) {
        	if (rect == null) continue;
        	// here we need to check only big boxes
        	if (!Utils.almost(rect.size.area(), _min_area*4)) continue;
        	
        	for (RotatedRect other_rect : _all_squares) {
        		if (other_rect == null) continue;
        		// here we need to check only small boxes
        		if (!Utils.almost(other_rect.size.area(), _min_area)) continue;
        		
        		//draw(mat, other_rect);
        		
        		Point[] rpts = new Point[4];
        		rect.points(rpts);
        		//System.out.println("in box:"+Utils.in_box(other_rect.center, rpts));
        		if (!Utils.in_box(other_rect.center, rpts)) continue;
        		//System.out.println("full:"+Utils.full(mat, rect));
        		if (!Utils.full(_mat, rect)) continue;
        		
        		Tag tag = new Tag(_fz, _mat, rect, other_rect.center);
        		tag.init();
        		tags.add(tag);
	            break;
        	}
        	
            if (tags.size() >= _max_tags) break;
        }
		return tags;
	}

}
