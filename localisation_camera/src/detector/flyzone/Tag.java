package detector.flyzone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import detector.Platform;
import detector.Utils;

public class Tag {
	
	public double[] _orient;
	public RotatedRect _rect;
	private FlyZone _fz;
	private Point _o; // orientation center
	private Mat _mat;
	private Mat _submat;
	private Point _value = null;
	private double s;

	public Tag(FlyZone fz, Mat mat, RotatedRect rect, Point o) {
		_fz = fz;
		_o = o;
		_rect = rect;
		_mat = mat;
	}
	
	public void init() {
		_orient = new double[]{_o.x-_rect.center.x, _o.y-_rect.center.y};
		s = Utils.length(_rect.size.height, _rect.size.width, 0, 0)/2;
		int rowStart = (int)(_rect.center.y-s);
		int rowEnd = (int)(_rect.center.y+s);
		int colStart = (int)(_rect.center.x-s);
		int colEnd = (int)(_rect.center.x+s);
		_submat = _mat.submat(rowStart, rowEnd, colStart, colEnd);
	}
	
	public Point get_center() {
		return _rect.center;
	}
	
	public double get_area() {
		return _rect.size.area();
	}

	public double get_angle() {
		// vector's lenght
        double l = Utils.length(new Point(_orient[0], _orient[1]));

        // direct vetor
        Point d = new Point(0, -l);

        double cos = (d.x*_orient[0]+d.y*_orient[1])/(l*l);

        double angle = Math.acos(cos)*180/Math.PI;

        // sign
        if (_orient[0] < 0) angle = -angle;

        // rotate on 135 degs counterclockwise
        if (angle > -45 && angle <= 180) angle = angle - 135;
        else if (angle >= -180 && angle <= -45) angle = 225 + angle;
        return angle;
	}

	public void draw_on(Mat mat) {
		//if not self._value:
        //    self._value = self.get_value_from(img, s)
        //if not self._value or len(self._value) != 2:
        //    return
		if (_value == null) return;
		List<MatOfPoint> cnts = new ArrayList<MatOfPoint>();
		Point[] pts = new Point[4];
		_rect.points(pts);
		cnts.add(new MatOfPoint(pts));
		Imgproc.drawContours(mat, cnts, -1, new Scalar(0, 0, 255));
        
		Core.line(mat, _rect.center, new Point(_orient[0]+_rect.center.x, _orient[1]+_rect.center.y), new Scalar(0, 255, 0)); 
        String txt = "null";
		if (_value != null) txt = _value.toString();
		Core.putText(mat, txt, _rect.center, Core.FONT_HERSHEY_TRIPLEX, 0.5, new Scalar(0, 0, 0));
	}

	public Point get_value() {
		//if self._value is not None:
	    //        return self._value
		//double s = _rect.size.height;
	    Point o = new Point(_orient[0]+s, _orient[1]+s);
	    
	    Point[] pts = new Point[4];
		_rect.points(pts);
		
		double min_len = Utils.length(pts[0].x-_rect.center.x+s,
									  pts[0].y-_rect.center.y+s,
									  o.x, o.y);
		for (int i = 1; i < 4; i++) {
			double len = Utils.length(pts[i].x-_rect.center.x+s,
						 			  pts[i].y-_rect.center.y+s,
						 			  o.x, o.y);
			if (min_len > len) min_len = len;
		}
		
		float mean0 = Utils.mean_hsv(_submat, new Point((int)o.x, (int)o.y), (int)min_len);
		int roi = (int)(_rect.size.height/6);
		// rotate an orientation vector to 90/180/270 degrees and take a mean hsv
		
		// coords wrt submat: o.x;o.y
		// coords wrt center: o.x-s; o.y-s
		// 1) rotate 90cw wrt center: -o.y+s; o.x-s
		// 2) convert to coords wrt submat: -o.y+s+s;o.x-s+s
		// 3) move closer to center: (-o.y+s)*0.75+s;(o.x-s)*0.75+s
		float mean1 = Utils.mean_hsv(_submat, new Point((int)((-o.y+s)*0.75+s),  (int)((o.x-s)*0.75+s)), roi);
		float mean2 = Utils.mean_hsv(_submat, new Point((int)((-o.x+s)*0.75+s),  (int)((-o.y+s)*0.75+s)), roi);
		float mean3 = Utils.mean_hsv(_submat, new Point((int)((o.y-s)*0.75+s),  (int)((-o.x+s)*0.75+s)), roi);
		
		int[] v = new int[]{-1, -1, -1, -1};
		for (int i=0; i < _fz._COLORS.length; i++) {
			if (mean0 <= ((Scalar)_fz._COLORS[i].get("upper")).val[0] &&
				mean0 >= ((Scalar)_fz._COLORS[i].get("lower")).val[0]) v[0] = i;
			if (mean1 <= ((Scalar)_fz._COLORS[i].get("upper")).val[0] &&
				mean1 >= ((Scalar)_fz._COLORS[i].get("lower")).val[0]) v[1] = i;
			if (mean2 <= ((Scalar)_fz._COLORS[i].get("upper")).val[0] &&
				mean2 >= ((Scalar)_fz._COLORS[i].get("lower")).val[0]) v[2] = i;
			if (mean3 <= ((Scalar)_fz._COLORS[i].get("upper")).val[0] &&
				mean3 >= ((Scalar)_fz._COLORS[i].get("lower")).val[0]) v[3] = i;
		}
		for (int i = 0; i < 4; i++) if (v[i] == -1) return null;
		if (Platform.MACOS) {
			Core.circle(_submat, new Point((int)o.x, (int)o.y), (int)min_len, new Scalar(0, 0, 0));
			Core.circle(_submat, new Point((int)((-o.y+s)*0.7+s),  (int)((o.x-s)*0.75+s)), roi, new Scalar(0, 255, 0));
			Core.circle(_submat, new Point((int)((-o.x+s)*0.75+s),  (int)((-o.y+s)*0.75+s)), roi, new Scalar(255, 0, 0));
			Core.circle(_submat, new Point((int)((o.y-s)*0.75+s),  (int)((-o.x+s)*0.75+s)), roi, new Scalar(0, 0, 255));
		}
		// there is a color red_2 that has number 6, but in fact it's 5
		String value = "";
		for (int i = 0; i < v.length; i++) {
			//if (v[i] == 6) v[i] = 5;
			value += " "+v[i];
		}
		_value = _fz._COORDS.get(value);
		//if (_value != null && _value.x == 4 && _value.y == 15)
		//	System.out.println(mean0+"\n-"+mean1+"\n--"+mean2+"\n---"+mean3);
		return _value;
	}

}
