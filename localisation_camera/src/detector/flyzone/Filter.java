package detector.flyzone;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.imgproc.Imgproc;

import detector.Utils;

public class Filter implements Callable< List<RotatedRect> >{
	
	private List<MatOfPoint> _points;
	private double _thresh;

	public Filter(List<MatOfPoint> points, double thresh) {
		_points = points;
		_thresh = thresh;
	}

	@Override
	public List<RotatedRect> call() throws Exception {
        List<RotatedRect> meaningful_squares = new ArrayList<RotatedRect>();
        for (MatOfPoint cnt : _points) {
        	RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
        	//double area = Imgproc.contourArea(cnt);
        	if (rrect.size.area() < _thresh) continue;
        	//RotatedRect rrect = Imgproc.minAreaRect(new MatOfPoint2f(cnt.toArray()));
        	if (Utils.almost_rectangular(rrect)) {
        		/*
        		if (min_cnt == null || min_area > rrect.size.area()) {
        			/*
        			Point[] rpts = new Point[4];
            		rrect.points(rpts);
            		// skip the contour sticked to the borders
                	// as we need nice and solid rectangles from the center
                	boolean skip = false;
                	for (int i = 0; i < rpts.length; i++)
                		if (rpts[i].x <= 10 || rpts[i].y <= 10 ||
                			rpts[i].x >= mat.cols()-10 || rpts[i].y >= mat.rows()-10) {
                			skip = true;
                			break;
                		}
                	
        			if (!skip) {
        				min_area = rrect.size.area();
        				min_cnt = cnt;
        			//}
        		}*/
        		meaningful_squares.add(rrect);
        	}
        }
		return meaningful_squares;
	}
}
