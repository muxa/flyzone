package detector;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.swing.JFrame;

import network.client.AttitudeListener;
import network.client.UDPClient;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import detector.aprilzone.AprilZone;
import detector.flyzone.FlyZone;
import stream.player.Videosrc;

public class Detector implements Runnable {
	
	private static final Logger log = Logger.getLogger( Detector.class.getName() );

	private VideoFrame vframe;
	
	private Point circle = new Point(-10, -10);
	private boolean _calibration = false;
	private boolean _visualize = false;
	
	private double roll;
	private double pitch;
	private boolean _stop = false;
	private JFrame window = null;
	private Mat frame = Mat.zeros(Configuration.getWidth(),
								  Configuration.getHeight(),
								  CvType.CV_8UC3);
	
	private Videosrc videosrc = null;

	private AprilZone az;

	private UDPClient client;

	private boolean _ready = false;

	private long _quasi_ip = 0;
	private volatile double[] _coords = new double[]{0, 0, 0};

	private Timer timer = null;
	
	public Detector(boolean calibration, boolean visualize) {
		init(calibration, visualize);
	}
	
	private void init(boolean calibration, boolean visualize) {
		_calibration = calibration;
		_visualize = visualize;
		
		if (_calibration || _visualize) createGUI();
		
		_ready = true;
	}
	
	public void setVideosrc(Videosrc input) {
		if (videosrc == null) videosrc = input;
		else videosrc.stop();
		videosrc = input;
		(new Thread(videosrc)).start();
	}
	
	public synchronized void setUDPClient(UDPClient c) {
		client = c;
		
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run() {
				client.send(_coords, _quasi_ip);
			}
		}, 0, 30);
	}
	
	public synchronized void stop() {
		_stop = true;
	}
	
	public synchronized void updateAttitude(double r, double p) {
		roll = r;
		pitch = p;
		//System.out.println("roll:"+roll+" pitch:"+pitch);
	}
	
/*	
	@Override
	public synchronized void frameUpdated(final BufferedImage im) {
		log.info("Updated");
		// TODO Auto-generated method stub
				frame = Utils.im2mat(im, CvType.CV_8UC3);
		        calibrate();
	}
	double thresh = 100;
    double t_s = 50;
    double gray = 150;
    double gray_st = 1;
    long t1;
    long t2;
    Point[] fps = new Point[100];
    Point[] pts = new Point[100];
    int count = 0;
    double k = 0;
    double k_max = 0.45;
    double[] coords = null;
    double[] prev = new double[3];
    int max_tags = 10;
    double[] target = new double[]{0.5, 0.5};
	private void process() {
		switch (Configuration.getMethod()) {
		case Configuration.APRILTAGS:
            t1 = System.currentTimeMillis();
            coords = az.process(frame.submat((int)(frame.rows()*k), (int)(frame.rows()*(1-k)),
						 (int)(frame.cols()*k), (int)(frame.cols()*(1-k))), target, true);
            t2 = System.currentTimeMillis();
            break;
		case Configuration.COLORTAGS:
            t1 = System.currentTimeMillis();
            coords = ((FlyZone)az).process_parallel(frame.submat((int)(frame.rows()*k), (int)(frame.rows()*(1-k)),
            							   						 (int)(frame.cols()*k), (int)(frame.cols()*(1-k))),
            							   			max_tags, target, thresh, gray, true);

            if (coords == null && thresh <= frame.rows()/5) {
                    t_s = frame.rows()/20;
            }
            if (coords == null && thresh >= frame.rows()/1.5) {
                    t_s = -frame.rows()/20;
            }
            if (coords == null) thresh += t_s;
            if (coords == null && gray <= 140) gray_st=5;
            if (coords == null && gray >= 150) gray_st=-5;
            if( coords == null) gray+=gray_st;
            t2 = System.currentTimeMillis();
            break;
		}
		//System.out.println(max_tags);
		fps[count % fps.length] = new Point(1000/(fps[count % fps.length].x+t2-t1), 0);
		
		if (fps[count % fps.length].x < 30 && k < 0.25) k+=0.05;
		if (fps[count % fps.length].x > 35 && k > 0.05) k-=0.05;
		
		if (coords == null && k > 0.1) {
			k-=0.1;
		}
		
		if (fps[count % fps.length].x < 30 && max_tags > 4) {
			max_tags--;
		}
		if (fps[count % fps.length].x > 35) max_tags++;
		
		if (coords == null && k > 0.1) {
			if (k_max > 0.1) k_max -= 0.1;
			k-=0.1;
		}
        if (coords == null) {
        	coords = prev;    
        } else {
        	prev = coords;
        }
        pts[count % pts.length] = new Point(coords[0], coords[1]);
        
        client.send(coords);
        
        double dp = Math.sqrt(Math.pow(frame.cols(), 2)+Math.pow(frame.rows(), 2));
		target = new double[]{0.5+dp*Math.tan(roll*Math.PI/180000)/(1.26*frame.cols()), // roll
							  0.5+dp*Math.tan(pitch*Math.PI/180000)/(1.26*frame.rows())}; // pitch
        
        count++;
        if (count > 0 && count % pts.length == 0) {
        	System.out.println("-----["+Utils.mean(Arrays.asList(pts))+Utils.std_dev(Arrays.asList(pts))+"]-----");
        	System.out.println("-----["+Utils.mean(Arrays.asList(fps)).x+"\t"+Utils.std_dev(Arrays.asList(fps)).x+"]-----");
        }
		
		
		//Highgui.imwrite(""+coords[0]+"_"+coords[1]+"_"+coords[2]+".png", frame);
		
		if (Platform.RPI || !_visualize) return;//continue;
		
		//System.out.println("pitch:"+_pitch+" yaw:"+coords[2]+" lpx:"+lpx+" lpy:"+lpy);
		Core.circle(frame, new Point(frame.cols()*target[0], frame.rows()*target[1]), 10, new Scalar(0, 0, 0), 3);
		// displaying the frame
		vframe.update_image(Utils.mat2im(frame));
	}
*/
	@Override
	public void run() {
		while(!_stop){
			if (_calibration) calibrate();
			else process();
		}
		// here _stop is true
		if (window != null) window.dispose();
		if (videosrc != null) videosrc.stop();
		if (timer != null) timer.cancel();
	}

	private void process() {
		
		AprilZone az = new AprilZone(new File("solution.txt").getAbsolutePath(), 4);
		az.initialize();
		
		double thresh = 100;
        double t_s = 50;
        double gray = 150;
        double gray_st = 1;
        long t1;
        long t2;
        Point[] fps = new Point[100];
        Point[] pts = new Point[100];
        int count = 0;
        double k = 0;
        double k_max = 0.45;
        double[] coords = null;
        double[] prev = new double[3];
        int max_tags = 10;
        double[] target = new double[]{0.5, 0.5};
		// processing
		while(!_stop) {
			//Mat.zeros(640, 480, CvType.CV_8UC3);
			t1 = System.currentTimeMillis();
			frame = videosrc.getImage();
			t2 = System.currentTimeMillis();
            fps[count % fps.length] = new Point(t2-t1, 0);
            coords = null;
            
			switch (Configuration.getMethod()) {
			case Configuration.APRILTAGS:
                t1 = System.currentTimeMillis();
                coords = az.process(frame.submat((int)(frame.rows()*k), (int)(frame.rows()*(1-k)),
  						 (int)(frame.cols()*k), (int)(frame.cols()*(1-k))), target, true);
                t2 = System.currentTimeMillis();
                break;
			case Configuration.COLORTAGS:
                t1 = System.currentTimeMillis();
                coords = ((FlyZone)az).process_parallel(frame.submat((int)(frame.rows()*k), (int)(frame.rows()*(1-k)),
                							   						 (int)(frame.cols()*k), (int)(frame.cols()*(1-k))),
                							   			max_tags, target, thresh, gray, true);
                //System.out.println(coords);
                if (coords == null && thresh <= frame.rows()/5) {
                        t_s = frame.rows()/20;
                }
                if (coords == null && thresh >= frame.rows()/1.5) {
                        t_s = -frame.rows()/20;
                }
                if (coords == null) thresh += t_s;
                if (coords == null && gray <= 140) gray_st=5;
                if (coords == null && gray >= 150) gray_st=-5;
                if( coords == null) gray+=gray_st;
                t2 = System.currentTimeMillis();
                break;
			}
			//System.out.println(max_tags);
			fps[count % fps.length] = new Point(1000/(fps[count % fps.length].x+t2-t1), 0);
			
			if (fps[count % fps.length].x < 30 && k < 0.25) k+=0.05;
			if (fps[count % fps.length].x > 35 && k > 0.05) k-=0.05;
			
			if (coords == null && k > 0.1) {
				k-=0.1;
			}
			
			if (fps[count % fps.length].x < 30 && max_tags > 4) {
				max_tags--;
			}
			if (fps[count % fps.length].x > 35) max_tags++;
			
			if (coords == null && k > 0.1) {
				if (k_max > 0.1) k_max -= 0.1;
				k-=0.1;
			}
            
            if (coords == null) {
            	coords = prev;    
            } else {
            	prev = coords;
            }
            //System.out.println("Decoded "+coords[0]+" "+coords[1]+" "+coords[2]);
            
            _coords[0] = coords[0];
            _coords[1] = coords[1];
            _coords[2] = coords[2];
            
            pts[count % pts.length] = new Point(coords[0], coords[1]);
            
            double dp = Math.sqrt(Math.pow(frame.cols(), 2)+Math.pow(frame.rows(), 2));
			target = new double[]{0.5+dp*Math.tan(roll*Math.PI/180)/(1.26*frame.cols()), // roll
								  0.5+dp*Math.tan(pitch*Math.PI/180)/(1.26*frame.rows())}; // pitch
            
            count++;
            if (count > 0 && count % pts.length == 0) {
            	System.out.println("-----["+Utils.mean(Arrays.asList(pts))+Utils.std_dev(Arrays.asList(pts))+"]-----");
            	System.out.println("-----["+Utils.mean(Arrays.asList(fps)).x+"\t"+Utils.std_dev(Arrays.asList(fps)).x+"]-----");
            }
			
			
			//Highgui.imwrite(""+coords[0]+"_"+coords[1]+"_"+coords[2]+".png", frame);
			
			if (Platform.RPI || !_visualize) continue;
			
			//System.out.println("pitch:"+_pitch+" yaw:"+coords[2]+" lpx:"+lpx+" lpy:"+lpy);
			//System.out.println(target[0]+" "+target[1]+" roll:"+roll+" pitch:"+pitch);
			Core.circle(frame, new Point(frame.cols()*target[0], frame.rows()*target[1]), 10, new Scalar(0, 0, 0), 3);
			// displaying the frame
			vframe.update_image(Utils.mat2im(frame));
			
		}
	}
	
	private void calibrate() {
		frame = videosrc.getImage();
		float mean = Utils.mean_hsv(frame, circle, 10);
		//System.out.println(mean);
		Core.circle(frame, circle, 10, new Scalar(0, 0, 0));
		vframe.update_image(Utils.mat2im(frame));
	}
	
	// creates a GUI
	private void createGUI()  {
		window = new JFrame("Calibration");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setBounds(0, 0, Configuration.getWidth(), Configuration.getHeight());
			
		vframe = new VideoFrame();
			
		vframe.addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				int x=e.getX();
				int y=e.getY();
				circle = new Point(x, y);
				//System.out.println(x+","+y);//these co-ords are relative to the component
			}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}
		});
			
		window.getContentPane().add(vframe);
		window.setVisible(true);
	}

	public void setLongIp(long ip) {
		_quasi_ip = ip;
	}

	
}
