package detector.aprilzone;


public class TagDetector36h11 {
	
	static {
		// initialize AprilTags
		System.loadLibrary("detector"); // Load native library at runtime
	}
	
	// initializes the detector
	private native void init();
	// returns found ids, coordinates (x, y), homography matrix index = [0<row<2]*3 + [0<col<2], and coordinates of p[4][2] index=[0<row<3]*2+[0<col<1]
	// id1, x1, y1, r11, r12, r13, r21, r22, r23, r31, r32, r33, p11, p12, p21, p22, p31, p32, p41, p42
	// id2, x2, y2, ...
	// ...
	private native double[][] detect(byte[] bs);
	// cleans the memory allocated in init()
	private native void clean();

	
	public void invoke_init() {
		init();
	}
	
	public double[][] invoke_detect(byte[] buf) {
		return detect(buf);
	}
	
	public void invoke_clean() {
		clean();
	}

}
