package detector.aprilzone;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import detector.Platform;
import detector.Utils;
import detector.flyzone.FlyZone;
import detector.flyzone.Tag;


public class AprilZone extends FlyZone{
	
	private TagDetector36h11 _detector;
	
	public AprilZone(String solution, int threads) {
		super(solution, threads);
		_detector = new TagDetector36h11();
	}

	public void	initialize() {
		super.initialize();
		_detector.invoke_init();
	}
	
	public double[] process(Mat frame, double[] target, boolean multithread) {
		MatOfByte buf = new MatOfByte();
		Highgui.imencode(".pnm", frame, buf);
		
		double[][] result = _detector.invoke_detect(buf.toArray());
		if (result.length == 0) return null;
		// TODO:
		//double pitch = 0;
		//double roll = 0;
		//double yaw = 0;
		List<List<Tag> > tags = new ArrayList<List<Tag> >();
        for (int i = 0; i < _threads; i++) {
        	List<Tag> tags_i = new ArrayList<Tag>();
        	tags.add(tags_i);
        }
        double mean_angle = 0;
        double mean_area = 0;
        for (int i = 0; i < result.length; i++) {
        	if (Platform.MACOS) {
        		Core.line(frame, new Point(result[i][3+0*2+0], result[i][3+0*2+1]), new Point(result[i][3+1*2+0], result[i][3+1*2+1]), new Scalar(255, 255, 255));
        		Core.line(frame, new Point(result[i][3+1*2+0], result[i][3+1*2+1]), new Point(result[i][3+2*2+0], result[i][3+2*2+1]), new Scalar(0, 255, 0));
        		Core.line(frame, new Point(result[i][3+2*2+0], result[i][3+2*2+1]), new Point(result[i][3+3*2+0], result[i][3+3*2+1]), new Scalar(255, 0, 0));
        		Core.line(frame, new Point(result[i][3+3*2+0], result[i][3+3*2+1]), new Point(result[i][3+0*2+0], result[i][3+0*2+1]), new Scalar(0, 0, 255));
			
        		Core.putText(frame, ""+(int)result[i][0]+":"+(int)result[i][0] % 24+","+(int)result[i][0]/24,
							new Point(result[i][1], result[i][2]), Core.FONT_HERSHEY_SIMPLEX, 0.6, new Scalar(0, 255, 0));
        		Core.line(frame, new Point(result[i][1], result[i][2]), new Point(result[i][1]+50*result[i][3+8 + 1*3+0], result[i][2]+50*result[i][3+8 + 0*3+0]), new Scalar(0, 255, 0));
        	}
			
			Point v21 = new Point(result[i][3+1*2+0]-result[i][3+2*2+0], result[i][3+1*2+1]-result[i][3+2*2+1]);
			Point v30 = new Point(result[i][3+0*2+0]-result[i][3+3*2+0], result[i][3+0*2+1]-result[i][3+3*2+1]);
			
			Point direction = new Point(v21.x/4+v30.x/4, v21.y/4+v30.y/4);
			double l = Utils.length(direction);
			Point vertical = new Point(0, -l);
			double cos = (vertical.x*direction.x+vertical.y*direction.y)/(l*l);
	        double angle = Math.acos(cos)*180/Math.PI;
	        // sign
	        if (direction.x < 0) angle = -angle;
	        
	        // area = 0.5*|(x1y2-y1x2)+(x2y3-y2x3)+...+(xny1+ynx1)|
	        double area = 0.5*Math.abs((result[i][3+0*2+0]*result[i][3+1*2+1]-result[i][3+0*2+1]*result[i][3+1*2+0])+
	        					   	   (result[i][3+1*2+0]*result[i][3+2*2+1]-result[i][3+1*2+1]*result[i][3+2*2+0])+
	        					   	   (result[i][3+2*2+0]*result[i][3+3*2+1]-result[i][3+2*2+1]*result[i][3+3*2+0])+
	        					   	   (result[i][3+3*2+0]*result[i][3+0*2+1]-result[i][3+3*2+1]*result[i][3+0*2+0]));
			
			AprilTag tag = new AprilTag(this);
			tag._angle = angle;
			tag._id = (int)result[i][0];
			tag._c = new Point(result[i][1], result[i][2]);
			tags.get(i % _threads).add(tag);
		
			mean_angle += angle;
			mean_area += area;
			// TODO:
			//pitch += Math.atan2(result[i][3 + 2*3+1], result[i][3 + 2*3+2]);
			//roll += Math.atan2(-result[i][3 + 2*3+0], Math.sqrt(Math.pow(result[i][3 + 2*3+1],2) + Math.pow(result[i][3 + 2*3+2],2)));
			//yaw += Math.atan2(result[i][3 + 1*3+0],result[i][3 + 0*3+0]);
		}
        
        if (result.length > 0) {
            mean_angle = mean_angle/result.length;
            mean_area = mean_area/result.length;
    	}
        
        estimate(frame.rows(), frame.cols(), target, mean_area, tags, multithread);
        if (_coords != null) _coords[2] = mean_angle;
		return _coords;
	}
	
	public void clean() {
		_detector.invoke_clean();
	}

}
