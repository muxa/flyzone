package detector.aprilzone;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;

import detector.flyzone.FlyZone;
import detector.flyzone.Tag;


public class AprilTag extends Tag {

	public double _angle = 0;
	public int _id = -1;
	public Point _c;
	
	public AprilTag(FlyZone fz) {
		super(fz, new Mat(640, 480, CvType.CV_8UC3), null, new Point(0, 0));
	}
	
	public double get_angle() {
		return _angle;
	}
	
	public Point get_value() {
		return new Point(_id % 24, _id / 24);
	}
	
	public Point get_center() {
		return _c;
	}

}
