package detector;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class FileRegistry {
	
	private static HashMap<String, BufferedWriter> _writers = new HashMap<String, BufferedWriter>();
	private static final String USER_DIR = System.getProperty("user.dir")+"/";
	
	public static void open(String name, boolean append) {
		try {
			_writers.put(name, new BufferedWriter(new FileWriter(USER_DIR+name, append)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void writeTo(String name, String data) {
		if (!_writers.containsKey(name)) open(name, false);
		try {
			_writers.get(name).write(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void close(String name) {
		if (!_writers.containsKey(name)) return;
		try {
			_writers.get(name).flush();
			_writers.get(name).close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void closeAll() {
		for (String name:_writers.keySet()) close(name);
	}

}
