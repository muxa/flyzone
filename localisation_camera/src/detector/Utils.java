package detector;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class Utils {
	
	public static Mat im2mat(BufferedImage im, int type) {
		int[] pixels = ((DataBufferInt) im.getRaster().getDataBuffer()).getData();
		byte[] data = new byte[pixels.length*3];
		for(int i = 0; i < pixels.length; i++) {
			data[i*3+2] = (byte) ((pixels[i]>>16)&255);
			data[i*3+1] = (byte) ((pixels[i]>>8)&255);
			data[i*3] = (byte) ((pixels[i])&255);
		}
        Mat mat = new Mat(im.getHeight(), im.getWidth(), type);
        mat.put(0, 0, data);
        return mat;
	}
	
	public static BufferedImage mat2im(Mat mat) {
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2RGB);
		byte[] data = new byte[mat.rows() * mat.cols() * (int)(mat.elemSize())];
        mat.get(0, 0, data);
        int type;
        if(mat.channels() == 1) type = BufferedImage.TYPE_BYTE_GRAY;
        else type = BufferedImage.TYPE_3BYTE_BGR;
        BufferedImage im = new BufferedImage(mat.cols(),mat.rows(), type);
        im.getRaster().setDataElements(0, 0, mat.cols(), mat.rows(), data);
        return im;
	}

	// returns True if 'a' and 'b' are almost equal
    // (with the acceptable 'error')
	public static boolean almost(double a, double b) {
	    return almost(a, b, 0.3);
	}
	
	public static boolean almost(double a, double b, double error) {
	    return (a >= b*(1-error) && a <= b*(1+error));
	}

	// returns True if the 'box' is almost rectangular
    // see also 'almost()'
	public static boolean almost_rectangular(RotatedRect rrect) {
	    return almost(rrect.size.height, rrect.size.width);
	}
	
	public static boolean almost_rectangular(RotatedRect rrect, double error) {
	    return almost(rrect.size.height, rrect.size.width, error);
	}


	// retuns True if 'a' and 'b' are on the same side of the line defined by
	// 'p1' and 'p2'.
	// if a = (ax, ay) and b = (bx, by) and F(a)*F(b) > 0 then
	// a and b are on the same side of the line.
	// The equation of the line is F(tx, ty) = 0 = Atx+Bty+C
	public static boolean check(Point a, Point b, Point p1, Point p2){
		// line coefficients
		double A = p1.y-p2.y;
		double B = p2.x-p1.x;
		double C = p1.x*p2.y-p2.x*p1.y;
		return (A*a.x + B*a.y + C)*(A*b.x + B*b.y + C) > 0;
	}
	    		
	//returns True if 'p' is within the rectangular 'box'
	//        2   Check that:
	//       / \  1) points p and 2 are on the same side of 0-1,
	//      1  3  2) points p and 3 are on the same side of 1-2,
	//      \ /   3) points p and 0 are on the same side of 2-3,
	//       0    4) points p and 1 are on the same side of 3-0.
	public static boolean in_box(Point p, Point[] box) {
		// TODO Auto-generated method stub
		return (check(p, box[2], box[0], box[1]) && 
				check(p, box[3], box[1], box[2]) &&
				check(p, box[0], box[2], box[3]) &&
				check(p, box[1], box[3], box[0]));
	}

	public static double length(double ax, double ay, double bx, double by) {
		return Math.sqrt((ax-bx)*(ax-bx)+(ay-by)*(ay-by));
	}

	public static double length(Point p1, Point p2) {
		return length(p1.x, p1.y, p2.x, p2.y);
	}

	// Returns a mean color of the area with the center in 'c'
    //and radius 'r' in the image 'img' in HSV format.
	public static float mean_hsv(Mat mat, Point o, int radius) {
		/*
		Point start = new Point(o.x-0.7*radius, o.y-0.7*radius);
		Point end = new Point(o.x+0.7*radius, o.y+0.7*radius);
		//System.out.println(""+start+"->"+end+" "+mat.rows()+" "+mat.cols());
		float mean = 0;
		for (int i=(int)start.y; i < end.y; i++)
			for (int j = (int)start.x; j < end.x; j++) {
				mean += HSVfromRGB(mat.get(i, j))[0]*179;
			}
		mean = mean/(2*radius*radius);
		return mean;
		*/
		Mat mask = Mat.zeros(mat.rows(), mat.cols(), CvType.CV_8UC1);
		Core.circle(mask, o, radius, new Scalar(255, 255, 255));
		//Mat hsv_mat = new Mat(mat.rows(), mat.cols(), mat.type());
		//Imgproc.cvtColor(mat, hsv_mat, Imgproc.COLOR_RGB2HSV);
		//Scalar mean_bgr = Core.mean(mat, mask);
		return HSLfromBGR(Core.mean(mat, mask).val)[0]*179;
	}
	
	public static float[] HSLfromBGR(double[] ds) {

		//  Get RGB values in the range 0 - 1
		float b = (float) (ds[0]/255f);
		float g = (float) (ds[1]/255f);
		float r = (float) (ds[2]/255f);
	
		//  Minimum and Maximum RGB values are used in the HSL calculations
		float min = Math.min(r, Math.min(g, b));
		float max = Math.max(r, Math.max(g, b));
		
		//  Calculate the Hue
		float h = 0;
		if (max == min) 
			h = 0;
		else if (max == r)
			h = (( (g - b) / (max - min) / 6f) + 1) % 1;
		else if (max == g)
			h = ( (b - r) / (max - min) / 6f) + 1f/3f;
		else if (max == b)
			h = ( (r - g) / (max - min) / 6f) + 2f/3f;
	
		//  Calculate the Luminance
		float l = (max + min) / 2;
		//  Calculate the Saturation
		float s = 0;
		if (max == min)
			s = 0;
		else if (l <= .5f)
			s = (max - min) / (max + min);
		else
			s = (max - min) / (2 - max - min);
		
		float[] dest = new float[3];
		dest[0] = h;
		dest[1] = s;
		dest[2] = l;
		return dest;
	}

	// checks if the tag is not cut by the frame's boundaries
	// i.e. is fully displayed
	public static boolean full(Mat mat, RotatedRect rect) {
		double s = Utils.length(rect.size.height, rect.size.width, 0, 0)/2;
		int row_start = (int)(rect.center.y-s);
		int row_end = (int)(rect.center.y+s);
		int col_start = (int)(rect.center.x-s);
		int col_end = (int)(rect.center.x+s);
		
		return (row_start > -1 && row_start < mat.rows() && 
				col_start > -1 && col_start < mat.cols() &&
				row_end > -1 && row_end < mat.rows() &&
				col_end > -1 && col_end < mat.cols());
	}
	
	// checks if every element in s1 less or equal than ones in s2
	// returns false otherwise
	public static boolean compare_leq(Scalar s1, Scalar s2) {
		return (s1.val[0] <= s2.val[0]);
	}

	public static double length(Point p) {
		return length(p, new Point(0, 0));
	}
	
	public static Point mean(List<Point> pts) {
        double sum_x = 0.0;
        double sum_y = 0.0;
        for(Point p : pts) {
        	sum_x += p.x;
        	sum_y += p.y;
        }
        return new Point(sum_x/pts.size(), sum_y/pts.size());
    }
	
	public static Point median(List<Point> pts) {
		if (pts.size() == 0) return new Point(0, 0);
		if (pts.size() == 1) return pts.get(0);
		double[] x = new double[pts.size()];
		double[] y = new double[pts.size()];
		for (int i = 0; i < pts.size(); i++) {
			x[i] = pts.get(i).x;
			y[i] = pts.get(i).y;
		}
		Arrays.sort(x);
		Arrays.sort(y);
	    int middle = pts.size()/2;
	    if (pts.size()%2 == 1) {
	        return new Point(x[middle], y[middle]);
	    } else {
	        return new Point(x[middle-1]/2 + x[middle]/2, y[middle-1]/2+y[middle]/2);
	    }
	}

    public static Point variance(List<Point> pts) {
        Point mean = mean(pts);
        double temp_x = 0;
        double temp_y = 0;
        for(Point p :pts) {
            temp_x += (mean.x-p.x)*(mean.x-p.x);
            temp_y += (mean.y-p.y)*(mean.y-p.y);
        }
        return new Point(temp_x/pts.size(), temp_y/pts.size());
    }
    
    public static Point med_variance(List<Point> pts) {
        Point mean = median(pts);
        double temp_x = 0;
        double temp_y = 0;
        for(Point p :pts) {
            temp_x += (mean.x-p.x)*(mean.x-p.x);
            temp_y += (mean.y-p.y)*(mean.y-p.y);
        }
        return new Point(temp_x/pts.size(), temp_y/pts.size());
    }

    public static Point std_dev(List<Point> c_r) {
    	Point var = variance(c_r);
        return new Point(Math.sqrt(var.x), Math.sqrt(var.y));
    }
    
    public static Point med_std_dev(List<Point> c_r) {
    	Point var = med_variance(c_r);
        return new Point(Math.sqrt(var.x), Math.sqrt(var.y));
    }

}
