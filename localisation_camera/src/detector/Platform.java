package detector;

public class Platform {

	public static boolean RPI = System.getProperties().getProperty("os.name").equals("Linux") &&
			  					System.getProperties().getProperty("os.arch").equals("arm");
	public static boolean MACOS = System.getProperties().getProperty("os.name").equals("Mac OS X") &&
								  System.getProperties().getProperty("os.arch").equals("x86_64");
	public static boolean Linux = System.getProperties().getProperty("os.name").equals("Linux") &&
			  					  System.getProperties().getProperty("os.arch").equals("amd64");
}
