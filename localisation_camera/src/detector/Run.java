package detector;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.net.UnknownHostException;

import network.Protocol;
import network.client.AttitudeListener;
import network.client.UDPClient;
import network.server.ConnectionListener;
import network.server.UDPServer;
import stream.player.GStreamerRunner;
import stream.player.RasPiCamRunner;
import stream.player.Videosrc;

import org.freedesktop.gstreamer.Gst;
import org.opencv.core.Core;

public class Run {
	
	private static final Logger log = Logger.getLogger( Run.class.getName() );
	private static UDPClient client = null;
	
	
	static {
		// initialize OpenCV
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		// initialize GStreamer
		Gst.init();
	}
	
	private static HashMap<String, Detector> _detectors = new HashMap<String, Detector>();
	private static String localhost;

	public static void main(String[] args) throws IOException {
		
		Configuration.init(args);
		
		localhost = InetAddress.getLocalHost().getHostAddress();
		
		client = new UDPClient();
		client.setAttitudeListener(new AttitudeListener(){
			@Override
			public void attitudeUpdated(double r, double p, long ip) {
				String host = Platform.RPI ? localhost
										   : network.Utils.longToIp(ip);
				if (!_detectors.containsKey(host)) return;
				//log.info("got attitude for "+network.Utils.longToIp(ip));
				_detectors.get(host).updateAttitude(r, p);
			}
		});
		(new Thread(client)).start();

		if (Platform.RPI) decentralized_loop();
		else centralized_loop();
	}
	
	private static void decentralized_loop() throws UnknownHostException {
		log.log(Level.INFO, "------------[Decentralized mode]------------");
		
		Detector detector = new Detector(false, false); // calibration, visualization
		detector.setVideosrc(new RasPiCamRunner());
		detector.setUDPClient(client);
		log.info("localhost "+localhost);
		_detectors.put(localhost, detector);
		detector.run();
	}
	
	private static void centralized_loop() {
		log.log(Level.INFO, "------------[Centralized mode]------------");
		
		UDPServer server = new UDPServer(Configuration.getStreamControlPort());
		server.setConnectionListener(new ConnectionListener(){
			@Override
			public void connected(InetAddress address) {
				// register RPI on the controller station
				client.req_register_streamer(address.getHostAddress());
				
				Detector detector = new Detector(false, true);
				detector.setLongIp(network.Utils.ipToLong(address.getHostName()));
				int port = Protocol.getStreamingPort(address);
				log.info("assigned port "+port+" to "+address.getHostName());
				detector.setVideosrc(new GStreamerRunner(port, address.getHostAddress()));
				detector.setUDPClient(client);
				(new Thread(detector)).start();
				
				_detectors.put(address.getHostAddress(), detector);
			}
			@Override
			public void disconnected(InetAddress a) {
				_detectors.remove(a.getHostAddress()).stop();
				
				// unregister RPI on the controller station
				client.req_drop_streamer(a.getHostAddress());
			}
		});
		log.info("Start server...");
		(new Thread(server)).start();
		
		while(true){}
	}
}
