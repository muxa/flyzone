package detector;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;


public class VideoFrame extends JPanel{
	
	private BufferedImage image2;
	private Timer fpsTimer;
	private int fps;
	private JLabel l1;
	
	ActionListener fpsReader = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			l1.setText("FPS: " + fps);
			fps=0;
		}
	};

	public VideoFrame() {
		super();
		fpsTimer = new Timer(1000, fpsReader); //check number of pictures displayed every 1000 ms
		fpsTimer.start();
		fps = 0;
		CreateGUI();
	}
	
	public void CreateGUI(){
		l1 = new JLabel("FPS: ");
		this.setSize(640,480);
		this.add(l1);
		this.setLayout(new GridLayout(1,1));
	}
	
	public void update_image(BufferedImage image) {
		image2 = image;
		this.repaint();
		fps++;
	}
	
	public void paintComponent(Graphics g){ //invoked by repaint
		super.paintComponent(g);
		if(image2 != null){
			g.drawImage(image2, 0, 0, this);
		}
	}

}
