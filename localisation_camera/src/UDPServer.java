import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;


public class UDPServer implements Runnable {
	
	private boolean _stop = false;
	private int _port = 2016;
	private float[] _attitude = new float[3];
	private double[] _coords = null;
	private InetAddress _address = null;
	
	public UDPServer(String ip, int port) {
		_port = port;
		try {
			_address = InetAddress.getByName(ip);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public UDPServer(int port) {
		_port = port;
	}
	
	public UDPServer() {}
	
	public synchronized void stop(boolean stop) {
		_stop = stop;
	}
	
	public synchronized float[] send_n_recevie(double[] coords) {
		_coords = coords.clone();
		return _attitude.clone();
	}

	@Override
	public void run() {
		DatagramSocket serverSocket = null;
		try {
			serverSocket = new DatagramSocket(_port);
			serverSocket.setSoTimeout(15);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
        byte[] receiveData = new byte[2*Float.SIZE/Byte.SIZE];
     
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        ByteBuffer bb = null;
        
        long t0_millis = System.currentTimeMillis();
    	long t0_nanos = System.nanoTime();
		
		while(!_stop) {
			try {
				serverSocket.receive(receivePacket);
				
				if (_address == null) _address = receivePacket.getAddress();
				
				bb = ByteBuffer.wrap(receivePacket.getData());
			    
		    	_attitude[0] = bb.getFloat();
		    	_attitude[1] = bb.getFloat();

				if (_coords == null || _address == null) continue;

				byte[] data = new byte[3*Double.SIZE/Byte.SIZE+Long.SIZE/Byte.SIZE];
		    	bb = ByteBuffer.wrap(data);
		    	bb.putDouble(_coords[0]);
		    	bb.putDouble(_coords[1]);
		    	bb.putDouble(_coords[2]);
		    	bb.putLong(t0_millis*1000+(System.nanoTime()-t0_nanos)/1000);
		    	t0_millis = System.currentTimeMillis();
		    	t0_nanos = System.nanoTime();
				serverSocket.send(new DatagramPacket(data, data.length, _address, _port));
				_coords = null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		serverSocket.close();
	}

}
