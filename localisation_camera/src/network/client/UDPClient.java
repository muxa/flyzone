package network.client;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import detector.Configuration;
import network.PacketBuilder;
import network.Protocol;
import network.server.ConnectionListener;


public class UDPClient implements Runnable{

	private static final Logger log = Logger.getLogger( UDPClient.class.getName() );
	
	private DatagramSocket _socket = null;
	DatagramPacket _response = new PacketBuilder()
									.set_empty(Protocol.PAYLOAD_SIZE)
									.build();
	private InetAddress _server;
	private Timer _timer = new Timer();
	private TimerTask _tt = new TimerTask(){
		@Override
		public void run() {}
	};
	private int _port = Configuration.getLocalisationPort();
	
	private List<Long> _registerPending = new ArrayList<Long>();
	private List<Long> _dropPending = new ArrayList<Long>();
	
	private StreamStarter _ss = new StreamStarter(){
		@Override
		public void start(InetAddress address, int width, int height, int port) {
			// TODO Auto-generated method stub	
		}
	};
	private AttitudeListener _l = new AttitudeListener(){
		@Override
		public void attitudeUpdated(double roll, double pitch, long ip) {
			// TODO Auto-generated method stub	
		}
	};
	private ConnectionListener _cl = new ConnectionListener(){
		@Override
		public void connected(InetAddress address) {
			// TODO Auto-generated method stub	
		}
		@Override
		public void disconnected(InetAddress a) {
			// TODO Auto-generated method stub	
		}
	};
	private boolean _stop = false;
	private boolean _connected = false;

	private boolean _connectionPending = false;
	
	//UDPClient brodcasts registration requests
	// when the registration is accepted, client memorizes the address

	public UDPClient() {
	}
	
	public UDPClient(int port) {
		_port = port;
	}
	
	public synchronized void send(double[] coords, long quasi_ip) {
		if (!_connected) return;
		//_coords = coords.clone();
		try {
			if (quasi_ip == 0) _socket.send(Protocol.req_payload(_server, coords));
			else _socket.send(Protocol.req_streaming_payload(_server, coords, quasi_ip));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void ask_streaming() {
		if (!_connected) return;
		try {
			_socket.send(Protocol.req_streaming(_server));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void req_register_streamer(String ip) {
		long quasi_ip = network.Utils.ipToLong(ip);
		_dropPending.remove(quasi_ip);
		_registerPending.add(quasi_ip);
	}
	
	public synchronized void req_drop_streamer(String ip) {
		long quasi_ip = network.Utils.ipToLong(ip);
		_registerPending.remove(quasi_ip);
		_dropPending.add(quasi_ip);
	}

	public synchronized void setAttitudeListener(AttitudeListener l) {
		_l = l;
	}
	public synchronized void setStreamStarter(StreamStarter ss) {
		_ss = ss;
	}
	public synchronized void setConnectionListener(ConnectionListener cl) {
		_cl = cl;
	}
	
	public synchronized void stop() {
		_stop  = true;
	}
	
	private void parse(DatagramPacket response) {
		ByteBuffer bb = ByteBuffer.wrap(response.getData());
		int code = bb.getInt();
		if (!_connected && code != Protocol.RES_REG_ACKN) return;
		switch(code) {
		/*------------------------[CENTRALIZED MODE]------------------------*/
		case Protocol.RES_REG_ACKN:
			_connected = true;
			_connectionPending = false;
			_server = response.getAddress();
			_cl.connected(_server);
			log.info("Connected");
			break;
		case Protocol.RES_PAYLOAD:
		/*------------------------[DECENTRALIZED MODE]----------------------*/
		case Protocol.RES_STREAMING_PAYLOAD:
			// payload response contains attitude (roll and pitch) and ip
			// the latter is needed for centralized approach: this info is
			// used to send it over to the appropriate RPI
			_l.attitudeUpdated(bb.getDouble(), bb.getDouble(), bb.getLong());
			break;
		case Protocol.RES_STREAMING:
			log.info("Got streaming response...");
			// streaming response contains width, height, and streaming port
			_ss.start(_server, (int)bb.getDouble(), (int)bb.getDouble(), (int)bb.getDouble());
			break;
		case Protocol.RES_REG_STREAMER_ACKN:
			long quasi_ip = bb.getLong();
			// streamer is registered
			_registerPending.remove(quasi_ip);
			break;
		case Protocol.RES_DROP_STREAMER_ACKN:
			long q_ip = bb.getLong();
			// streamer is dropped
			_dropPending.remove(q_ip);
			break;
		default:
			// smth unexpected
			//log.info("Unknown response "+code);
			return;
		}
		// update timer
		_tt.cancel();
		_tt = new TimerTask(){
			@Override
			public void run() {
				_connected = false;
				_cl.disconnected(_server);
				log.info("Disconnected");
			}
		};
		_timer.schedule(_tt, Protocol.TIMEOUT_MS);
	}

	@Override
	public void run() {
		// initial setup
		try {
			_socket = new DatagramSocket(_port);
			_socket.setSoTimeout(Protocol.SO_TO_CLIENT_MS);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		// packets of this size are expected
		DatagramPacket response = new PacketBuilder()
									.set_empty(Protocol.PAYLOAD_SIZE)
									.build();
		_timer = new Timer();
		// schedule permanent timer for NAT-like behavior
		_timer.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run() {
				try{
					for (Long quasi_ip:_registerPending) _socket.send(Protocol.req_register_streamer(_server, quasi_ip));
					for (Long quasi_ip:_dropPending) _socket.send(Protocol.req_drop_streamer(_server, quasi_ip));
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
		}, 0, Protocol.SO_TO_CLIENT_MS);
		
		while(!_stop) {
			try {
				if(!_connected && !_connectionPending) {
					// schedule temporary timer for connection
					_connectionPending = true;
					_tt.cancel();
					_tt = new TimerTask() {
						@Override
						public void run() {
							try {
								_socket.send(Protocol.req_register(_port));
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					};
					_timer.scheduleAtFixedRate(_tt, 0, Protocol.SO_TO_CLIENT_MS);
				}
				
				_socket.receive(response);
				
				parse(response);
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		_socket.close();
		_timer.cancel();
		_connected = false;
		_connectionPending = false;
	}

}
