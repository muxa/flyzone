package network.client;

import java.net.InetAddress;

public interface StreamStarter {
	public void start(InetAddress address, int width, int height, int port);
}
