package network.client;

public interface AttitudeListener {
	public void attitudeUpdated(double roll, double pitch, long ip);
}
