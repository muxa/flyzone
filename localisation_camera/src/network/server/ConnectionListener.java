package network.server;

import java.net.InetAddress;

public interface ConnectionListener {
	public void connected(InetAddress address);
	public void disconnected(InetAddress a);
}
