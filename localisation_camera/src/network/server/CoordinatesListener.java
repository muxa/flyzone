package network.server;

public interface CoordinatesListener {
	public void coordinatesUpdated(String ip, double x, double y, double a);
}
