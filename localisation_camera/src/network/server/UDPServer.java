package network.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import detector.Configuration;
import network.PacketBuilder;
import network.Protocol;
import network.Utils;

public class UDPServer implements Runnable{
	
	private static final Logger log = Logger.getLogger( UDPServer.class.getName() );
	
	private HashMap<InetAddress, TimerTask> _connections = new HashMap<InetAddress, TimerTask>();
	
	// conversion from virtual address to real address: the 'virtual' addresses are hidden after the real one
	private HashMap<InetAddress, InetAddress> _virtualConnections = new HashMap<InetAddress, InetAddress>();
	
	private HashMap<InetAddress, double[]> _mq = new HashMap<InetAddress, double[]>();
	private boolean _stop = false;
	private DatagramSocket _socket;
	private CoordinatesListener _l = new CoordinatesListener(){
		@Override
		public void coordinatesUpdated(String ip, double x, double y, double a) {
			// TODO Auto-generated method stub
		}
	};
	private ConnectionListener _cl = new ConnectionListener(){
		@Override
		public void connected(InetAddress address) {
			// TODO Auto-generated method stub
		}
		@Override
		public void disconnected(InetAddress a) {
			// TODO Auto-generated method stub
			
		}
	};
	private int _port = Configuration.getLocalisationPort();
	
	public UDPServer(int port) {
		_port  = port;
	}

	public UDPServer() {}

	public synchronized void stop() {
		_stop = true;
	}
	
	public synchronized void setCoordinatesListener(CoordinatesListener l) {
		_l = l;
	}
	
	public synchronized void setConnectionListener(ConnectionListener connectionListener) {
		_cl = connectionListener;
	}
	
	public synchronized void send(InetAddress address, double roll, double pitch) {
		// sending only local number of ip
		long ip = Utils.ipToLong(address.getHostAddress());
		// pack it into double for transportation
		double quasi_ip = Double.longBitsToDouble(ip);
		log.info("put "+address.getHostAddress()+" roll:"+roll+" pitch:"+pitch+" ip:"+quasi_ip);
		_mq.put(address, new double[]{roll, pitch, quasi_ip});
	}
	
	public synchronized Set<InetAddress> connections() {
		return _connections.keySet();
	}

	@Override
	public void run() {
		log.info("Starting up server...");
		// initial setup
		try {
			_socket = new DatagramSocket(_port);
			_socket.setSoTimeout(0);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		// packets of this size are expected from clients
		DatagramPacket request = new PacketBuilder()
								.set_empty(Protocol.PAYLOAD_SIZE)
								.build();
		Timer timer = new Timer();
		while(!_stop) {
			try {
				_socket.receive(request);
				parse(request);
				
				// update timer
				final InetAddress adr = request.getAddress();
				if (_connections.containsKey(adr))
					_connections.get(adr).cancel();
				TimerTask tt = new TimerTask(){
					@Override
					public void run() {
						_connections.remove(adr);
						for (InetAddress a:_virtualConnections.keySet())
							_cl.disconnected(a);
						_virtualConnections.clear();
						_cl.disconnected(adr);
					}
				};
				_connections.put(adr, tt);
				timer.schedule(tt, Protocol.TIMEOUT_MS);
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		_socket.close();
	}
	
	private void parse(DatagramPacket req) throws IOException {
		final InetAddress adr = req.getAddress();
		ByteBuffer bb = ByteBuffer.wrap(req.getData());
		int command = bb.getInt();
		
		if (!_connections.containsKey(adr) && command != Protocol.REQ_REGISTER) {
			log.info("Address "+adr.getHostAddress()+" is not registered, refuse answering.");
		}
		
		// extract command
		switch(command) {
		/*-------------------------[CENTRALIZED MODE]-----------------------*/
		case Protocol.REQ_REGISTER:
			// register client if it's not registered already
			if (!_connections.containsKey(adr)) {
				_socket.send(Protocol.res_reg_ackn(adr, _port));
				log.info("Acknowledge connection...");
				_cl.connected(adr);
			}
			break;
		case Protocol.REQ_PAYLOAD:
			_l.coordinatesUpdated(adr.getHostAddress(),
								  bb.getDouble(),
								  bb.getDouble(),
								  bb.getDouble());
			log.info("Got coordinates... Send attitude...");
			if (_mq.containsKey(adr)) {
				double[] attitude = _mq.remove(adr);
				_socket.send(Protocol.res_payload(adr, attitude));
				log.info("roll:"+attitude[0]+" pitch:"+attitude[1]+" ip:"+attitude[2]);
			}
			break;
		/*------------------------[DECENTRALIZED MODE]----------------------*/
		case Protocol.REQ_REGISTER_STREAMER:
			// in this case the 'client' is virtual
			log.info("Acknowledge streamer...");
			long l_ip = bb.getLong();
			String s_ip = network.Utils.longToIp(l_ip);
			InetAddress address = InetAddress.getByName(s_ip);
			_virtualConnections.put(address, adr);
			_socket.send(Protocol.res_reg_streamer_ackn(adr, l_ip));
			_cl.connected(address);
			break;
		case Protocol.REQ_DROP_STREAMER:
			// in this case the 'client' is virtual
			log.info("Acknowledge drop...");
			long lip = bb.getLong();
			String sip = network.Utils.longToIp(lip);
			InetAddress vadr = InetAddress.getByName(sip);
			_virtualConnections.remove(vadr);
			_socket.send(Protocol.res_drop_streamer_ackn(adr, lip));
			_cl.disconnected(vadr);
			break;
		case Protocol.REQ_STREAMING_PAYLOAD:
			double x = bb.getDouble();
			double y = bb.getDouble();
			double a = bb.getDouble();
			long quasi_ip = bb.getLong();
			
			String ip = network.Utils.longToIp(quasi_ip);
			_l.coordinatesUpdated(ip, x, y, a);
			log.info("Got coordinates... Send attitude...");
			InetAddress dest = InetAddress.getByName(ip);
			if (!_virtualConnections.containsKey(dest)) {
				// green channel for unregistered virtual connections
				_virtualConnections.put(dest, adr);
				_cl.connected(InetAddress.getByName(ip));
			}
			log.info("dest "+dest.getHostAddress());
			if (_mq.containsKey(dest)) {
				double[] attitude = _mq.remove(dest);
				_socket.send(Protocol.res_streaming_payload(adr, attitude));
				log.info("roll:"+attitude[0]+" pitch:"+attitude[1]+" ip:"+attitude[2]);
			}
			break;
		case Protocol.REQ_STREAMING:
			_socket.send(Protocol.res_streaming(adr));
			log.info("Responce streaming...");
			break;
		default:
			// smth unknown
			//log.info("Unknown command "+command);
			return;
		}
	}

}
