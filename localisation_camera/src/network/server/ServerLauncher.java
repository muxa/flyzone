package network.server;

import java.net.InetAddress;

import network.Protocol;

public class ServerLauncher {
	
	public static void main(String[] args) {
		UDPServer server = new UDPServer();
		
		server.setCoordinatesListener(new CoordinatesListener(){
			@Override
			public void coordinatesUpdated(String ip, double x, double y, double a) {
				System.out.println(ip+" x:"+x+" y:"+y+" a:"+a);
			}
		});
		server.setConnectionListener(new ConnectionListener(){
			@Override
			public void connected(InetAddress address) {
				System.out.println("connected "+address.getHostAddress());
			}
			@Override
			public void disconnected(InetAddress a) {
				System.out.println("disconnected "+a.getHostAddress());
			}
		});
		
		(new Thread(server)).start();
		
		while(true){
			for (InetAddress a: server.connections())
				server.send(a, 42.0, 25.0);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
