package network;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import detector.Configuration;

public class PacketBuilder {
	
	private InetAddress _address = null;
	private int _port = Configuration.getLocalisationPort();
	private byte[] _payload = new byte[0];
	
	public DatagramPacket build() {
		if (_address == null) return new DatagramPacket(_payload, _payload.length);
		return new DatagramPacket(_payload, _payload.length, _address, _port);
	}
	
	public PacketBuilder set_address(String ip) {
		try {
			_address = InetAddress.getByName(ip);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public PacketBuilder broadcast() {
		try {
			_address = InetAddress.getByName(Configuration.getBroadcastIp());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public PacketBuilder set_address(InetAddress address) {
		_address = address;
		return this;
	}
	
	public PacketBuilder set_port(int port) {
		_port = port;
		return this;
	}
	
	public PacketBuilder set_payload(byte[] payload){
		_payload = payload;
		return this;
	}
	
	public PacketBuilder set_empty(int size){
		_payload = new byte[size];
		return this;
	}
	
	public PacketBuilder req_register(){
		_payload = ByteBuffer
					.allocate(Protocol.PAYLOAD_SIZE)
					.putInt(Protocol.REQ_REGISTER)
					.array();
		return this;
	}

	public PacketBuilder res_reg_ackn() {
		_payload = ByteBuffer
				.allocate(Protocol.PAYLOAD_SIZE)
				.putInt(Protocol.RES_REG_ACKN)
				.array();
		return this;
	}
	
	public PacketBuilder req_streaming() {
		_payload = ByteBuffer
				.allocate(Protocol.PAYLOAD_SIZE)
				.putInt(Protocol.REQ_STREAMING)
				.array();
		return this;
	}
}
