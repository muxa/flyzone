package network;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import detector.Configuration;
import stream.player.GStreamerRunner;

public class Protocol {
	
	private static final Logger log = Logger.getLogger( Protocol.class.getName() );
	
//-----------------------------[CONSTANTS]-----------------------------------//
	
	public static final long TIMEOUT_MS = 1000;
	public static final long STR_TIMEOUT_MS = 100000;
	public static final int SO_TO_CLIENT_MS = 3000; // client waits 30ms
	public static final int SO_TO_SERVER_MS = 1; // server waits 1ms
	
	public static int PAYLOAD_SIZE = (Integer.SIZE+4*Double.SIZE)/Byte.SIZE;
	
/*---------------------------------------------------------------------------*
 *                        decentralized mode                                 *
 *---------------------------------------------------------------------------*/
	
//-----------------------------[REQUESTS]------------------------------------//
	
	// request register
	public final static int REQ_REGISTER = 1;
	// send payload
	public final static int REQ_PAYLOAD = 2;
	
	public static DatagramPacket req_register(int port) {
		return new PacketBuilder()
					.req_register()
					.broadcast()
					.set_port(port)
					.build();
	}
	
	public static DatagramPacket req_payload(InetAddress address, double[] payload) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(REQ_PAYLOAD)
									.putDouble(payload[0])
									.putDouble(payload[1])
									.putDouble(payload[2])
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}

//-----------------------------[RESPONSES]-----------------------------------//
	
	// acknowledge register
	public final static int RES_REG_ACKN = 101;
	// attitude as a responce to payload
	public final static int RES_PAYLOAD = 103;
	
	public static DatagramPacket res_payload(InetAddress address, double[] payload) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(RES_PAYLOAD)
									.putDouble(payload[0])
									.putDouble(payload[1])
									.putDouble(payload[2])
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
	public static DatagramPacket res_reg_ackn(InetAddress address, int port) {
		return new PacketBuilder()
					.res_reg_ackn()
					.set_address(address)
					.set_port(port)
					.build();
	}
	
/*---------------------------------------------------------------------------*
 *                          centralized mode                                 *
 *---------------------------------------------------------------------------*/	
	
//-----------------------------[REQUESTS]------------------------------------//	
	
	// request streaming
	public final static int REQ_STREAMING = 3;
	// register streamer
	public final static int REQ_REGISTER_STREAMER = 4;
	// drop streamer
	public final static int REQ_DROP_STREAMER = 5;
	// streaming payload
	public final static int REQ_STREAMING_PAYLOAD = 6;
	
	public static DatagramPacket req_register_streamer(InetAddress address, Long quasi_ip) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(REQ_REGISTER_STREAMER)
									.putDouble(Double.longBitsToDouble(quasi_ip))
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
	public static DatagramPacket req_drop_streamer(InetAddress address, Long quasi_ip) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(REQ_DROP_STREAMER)
									.putDouble(Double.longBitsToDouble(quasi_ip))
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}

	public static DatagramPacket req_streaming(InetAddress address) {
		return new PacketBuilder()
					.set_address(address)
					.req_streaming()
					.set_port(Configuration.getStreamControlPort())
					.build();
	}
	
	public static DatagramPacket req_streaming_payload(InetAddress address, double[] payload, long ip) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(REQ_STREAMING_PAYLOAD)
									.putDouble(payload[0])
									.putDouble(payload[1])
									.putDouble(payload[2])
									.putDouble(Double.longBitsToDouble(ip))
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
//-----------------------------[RESPONSES]-----------------------------------//
	

	// acknowledge streaming
	public final static int RES_STREAMING = 102;
	// acknowledge register
	public final static int RES_REG_STREAMER_ACKN = 104;
	// acknowledge drop
	public final static int RES_DROP_STREAMER_ACKN = 105;
	// streaming payload
	public final static int RES_STREAMING_PAYLOAD = 106;
	
	public static DatagramPacket res_reg_streamer_ackn(InetAddress adr, long quasi_ip) {
		return new PacketBuilder()
					.set_address(adr)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(RES_REG_STREAMER_ACKN)
									.putDouble(Double.longBitsToDouble(quasi_ip))
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
	public static DatagramPacket res_drop_streamer_ackn(InetAddress adr, long quasi_ip) {
		return new PacketBuilder()
					.set_address(adr)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(RES_DROP_STREAMER_ACKN)
									.putDouble(Double.longBitsToDouble(quasi_ip))
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
	
	public static DatagramPacket res_streaming(InetAddress address) {
		int port = getStreamingPort(address);
		log.info("response streaming to port "+port+" from address "+address.getHostAddress());
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(RES_STREAMING)
									.putDouble(Configuration.getWidth())
									.putDouble(Configuration.getHeight())
									.putDouble(port)
									.array())
					.set_port(Configuration.getStreamControlPort())
					.build();
	}
	
	public static DatagramPacket res_streaming_payload(InetAddress address, double[] payload) {
		return new PacketBuilder()
					.set_address(address)
					.set_payload(ByteBuffer.allocate(PAYLOAD_SIZE)
									.putInt(RES_STREAMING_PAYLOAD)
									.putDouble(payload[0])
									.putDouble(payload[1])
									.putDouble(payload[2])
									.array())
					.set_port(Configuration.getLocalisationPort())
					.build();
	}
	
/*---------------------------------------------------------------------------*
 *                          handling ports                                   *
 *---------------------------------------------------------------------------*/	

	public static int getStreamingPort(InetAddress adr) {
		// port is statically assigned by a formula 5000 + last number of ip
		return Configuration.getStreamControlPort()+
				Integer.parseInt(adr.getHostAddress().split("\\.")[3]);
	}
}
