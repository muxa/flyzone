package stream.player;

import detector.Configuration;

public class RasPiCam {
	
	static {
		// initialize raspicam
		System.loadLibrary("raspicam"); // Load native library at runtime
	}
	
	// initialize camera
	private native void init(int width, int height);
	// get frame
	private native byte[] capture();
	// cleanup
	private native void clean();
	
	public RasPiCam() {
		init(Configuration.getWidth(), Configuration.getHeight());
	}
	
	public byte[] get_frame() {
		return capture();
	}
	public void cleanup() {
		clean();
	}

}
