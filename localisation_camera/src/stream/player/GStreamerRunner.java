package stream.player;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

import org.freedesktop.gstreamer.Bin;
import org.freedesktop.gstreamer.Buffer;
import org.freedesktop.gstreamer.Caps;
import org.freedesktop.gstreamer.Pipeline;
import org.freedesktop.gstreamer.Sample;
import org.freedesktop.gstreamer.Structure;
import org.freedesktop.gstreamer.elements.AppSink;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import detector.Configuration;
import detector.FileRegistry;
import detector.Utils;


public class GStreamerRunner implements Videosrc{
	
	private static final Logger log = Logger.getLogger( GStreamerRunner.class.getName() );
	
	
	private BufferedImage im = new BufferedImage(Configuration.getWidth(),
												Configuration.getHeight(),
												BufferedImage.TYPE_INT_RGB);
	private final Lock bufferLock = new ReentrantLock();
	private int _port;
	private String _ip = "";
	protected ByteBuffer bb;
	private boolean _stop = false;

	public GStreamerRunner (final int port) {
		_port = port;
	}
	
	public GStreamerRunner(int port, String ip) {
		_port = port;
		_ip = ip;
	}

	public synchronized Mat getImage() {
		return Utils.im2mat(im, CvType.CV_8UC3);
	}
	
	@Override
	public void run() {
		
		// TODO Auto-generated method stub
		log.info("Running streamer");
		Bin bin = Bin.launch("udpsrc port="+_port+" ! application/x-rtp, payload=96 ! rtpjitterbuffer ! rtph264depay ! avdec_h264 ! videoconvert", true);
        Pipeline pipe = new Pipeline();
        
        AppSink videosink = new AppSink("GstVideoComponent");
        videosink.set("emit-signals", true);
        //AppSinkListener sl = new AppSinkListener();
        //sl.setFrameListener(fl);
        videosink.connect(new AppSink.NEW_SAMPLE() {
        	private long _start_time = 0;
			@Override
			public void newBuffer(AppSink elem) {
				if (Configuration.getMeasureLatency()) {
					if (_start_time == 0) _start_time = System.currentTimeMillis();
					System.out.println("st:"+elem.getStartTime().toMillis()+" "+
					                   "bt:"+elem.getBaseTime().toMillis()+" "+
					                   "it:"+elem.getClock().getInternalTime().toMillis()+" "+
					                   "tt:"+elem.getClock().getTime().toMillis()+" "+
					                   "mt:"+(System.currentTimeMillis()-_start_time));
					FileRegistry.writeTo(_ip, ""+elem.getClock().getTime().toString()+
											  " "+System.currentTimeMillis()+"\n");
				}
		    	Sample sample = elem.pullSample();
		        Structure capsStruct = sample.getCaps().getStructure(0);
		        int w = capsStruct.getInteger("width");
		        int h = capsStruct.getInteger("height");
		        //log.info("get buffer");
		        Buffer buffer = sample.getBuffer();
		        //log.info("map buffer");
		         bb = buffer.map(false);
		        if (bb != null) {
		        	//log.info("try lock");
		        	if (bufferLock.tryLock()) {
		        		//try{
		        			//log.info("Locked...");
		        			if (im == null || im.getHeight() != h || im.getWidth() != w)
		        				im = new BufferedImage(w, h, BufferedImage.TYPE_INT_BGR);
		        			int[] pixels = ((DataBufferInt) im.getRaster().getDataBuffer()).getData();
		        			bb.asIntBuffer().get(pixels, 0, w * h);
		        			
		        			//log.info("got im w "+w+" h "+h+" ");
		        			
		        			//_fl.frameUpdated(im);
		        			//log.info("Frane updated...");
		        		//} finally {
		        			//log.info("unlocking");
		        			bufferLock.unlock();
		        		//}
		        	}
		        	//log.info("Unmapping...");
		        	buffer.unmap();
		        }
		        //log.info("Disposing...");
		        sample.dispose();
			}
		});
        StringBuilder caps = new StringBuilder("video/x-raw, ");
        // JNA creates ByteBuffer using native byte order, set masks according to that.
        if (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN) {
            caps.append("format=BGRx");
        } else {
            caps.append("format=xRGB");
        }
        videosink.setCaps(new Caps(caps.toString()));
        
        pipe.addMany(bin, videosink);
        Pipeline.linkMany(bin, videosink); 
        
        pipe.play();
        //Gst.main();
        while(!_stop){}
        pipe.stop();
        pipe.dispose();
	}
	
	public void stop() {
		_stop = true;
		FileRegistry.close(_ip);
	}
}
