package stream.player;

import java.awt.image.BufferedImage;

import org.opencv.core.Mat;

public interface Videosrc extends Runnable{
	public Mat getImage();
	public void stop();
}
