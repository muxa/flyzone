package stream.player;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Given an extended JPanel and URL read and create BufferedImages to be displayed from a MJPEG stream
 * @author shrub34 Copyright 2012
 * Free for reuse, just please give me a credit if it is for a redistributed package
 */
public class MjpegRunner implements Runnable
{
	private InputStream urlStream;
	private StringWriter stringWriter;
	private boolean processing = true;
	private byte[] _imageBytes = new byte[0];
	
	public MjpegRunner(URL url) throws IOException {
		URLConnection urlConn = url.openConnection();

		urlConn.connect();
		urlStream = urlConn.getInputStream();
		stringWriter = new StringWriter(128);
	}

	/**
	 * Stop the loop, and allow it to clean up
	 */
	public synchronized void stop() {
		processing = false;
	}
	
	public synchronized byte[] image() {
		return _imageBytes;
	}

	/**
	 * Keeps running while process() returns true
	 * 
	 * Each loop asks for the next JPEG image and then sends it to our JPanel to draw
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while(processing) {
			try {
				_imageBytes = retrieveNextImage();
			}catch(SocketTimeoutException ste){
				System.err.println("MjpegRunner error: " + ste);
				stop();
			}catch(IOException e){
				System.err.println("MjpegRunner read: " + e);
				stop();
			}
		}
		// close streams
		try {
			urlStream.close();
		}catch(IOException ioe){
			System.err.println("Failed to close the stream: " + ioe);
		}
	}
	
	/**
	 * Using the <i>urlStream</i> get the next JPEG image as a byte[]
	 * @return byte[] of the JPEG
	 * @throws IOException
	 */
	private byte[] retrieveNextImage() throws IOException
	{
		boolean haveHeader = false; 
		int currByte = -1;
		
		String header = null;
		while((currByte = urlStream.read()) > -1 && !haveHeader) //read bytes until header is found
		{
			stringWriter.write(currByte); //read bytes as string
			
			String tempString = stringWriter.toString(); 
			int indexOf = tempString.indexOf("Content-Length:"); //make sure to get content length in the header
			if (indexOf > 0 ) {
				String length = tempString.substring(indexOf);
				if (length.matches("Content-Length:\\s\\d+\\s+")) {
					header = length;
					haveHeader = true;
				}
			}
		}		
		
		// 255 indicates the start of the jpeg image data
		while((urlStream.read()) != 255){}
		
		// rest is the buffer
		int contentLength = Integer.parseInt(header.substring(header.indexOf("Content-Length:")+16).replace("\r", ""));
		byte[] imageBytes = new byte[contentLength + 1];
		// since we ate the original 255 , shove it back in
		
		imageBytes[0] = (byte)255;
		int offset = 1;
        int numRead = 0;
        while (offset < imageBytes.length
               && (numRead=urlStream.read(imageBytes, offset, imageBytes.length-offset)) >= 0) {
            offset += numRead;
        }       
		
		stringWriter = new StringWriter(128);
		
		return imageBytes;
	}
}