package stream.player;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.core.MatOfByte;

import detector.Utils;

public class RasPiCamRunner implements Runnable, Videosrc{
	
	private boolean _stop = false;
	private RasPiCam _cam = null;
	private byte[] _frame = new byte[0];
	
	public RasPiCamRunner() {
		_cam = new RasPiCam();
	}
	
	public synchronized void stop() {
		_stop = true;
	}
	
	@Override
	public synchronized Mat getImage() {
		byte[] data = _frame.clone();
		//System.out.println(new String(data));
		Mat frame = Highgui.imdecode(new MatOfByte(data), Highgui.IMREAD_ANYCOLOR);
                //Highgui.imwrite( "img.jpg", frame);
		return frame;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!_stop) {
			_frame = _cam.get_frame();
		}
		_cam.cleanup();
	}

}
