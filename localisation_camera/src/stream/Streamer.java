package stream;

import java.net.InetAddress;
import java.io.IOException;
import java.util.logging.Logger;

import network.Protocol;
import network.server.ConnectionListener;
import network.client.AttitudeListener;
import network.client.StreamStarter;
import network.client.UDPClient;

import java.util.Timer;
import java.util.TimerTask;

import detector.Configuration;

public class Streamer {

	private static final Logger log = Logger.getLogger( Streamer.class.getName() );
	private static Timer t = null;
	private static boolean scheduled = false;
	private static Process p = null;
	
	public static void main(String[] args) throws IOException {
		Configuration.init(args);
		
		t = new Timer();
		
		UDPClient client = new UDPClient(Configuration.getStreamControlPort());
		client.setAttitudeListener(new AttitudeListener(){
			@Override
			public void attitudeUpdated(double roll, double pitch, long ip) {}
		});
		client.setStreamStarter(new StreamStarter(){
			@Override
			public void start(InetAddress address, int width, int height, int port) {
                                //System.out.println("start streaming "+width+" "+height+" "+port+" "+address.getHostAddress());
				if (p != null) return;
				log.info("wait streaming...");
				String cmd = "/bin/sh "+System.getProperty("user.dir")+
					       	 "/scripts/gst.sh "+width+" "+height+
					       	 " "+address.getHostAddress()+" "+port;
				if(Configuration.getMeasureLatency())
					cmd = "/bin/sh "+System.getProperty("user.dir")+
			       	 	  "/scripts/gst_measure_latency.sh "+
			       	 	  " "+address.getHostAddress()+" "+port;
				final String command = cmd;
				//t.schedule(new TimerTask() {
				//	@Override
				//	public void run() {
						log.info("start streaming "+command);
                                		try {
                                        		p = Runtime.getRuntime().exec(command+" "+System.currentTimeMillis());
                                		} catch (IOException  e) {
                                        		e.printStackTrace();
                                		}
                                		log.info("started streaming");
						scheduled = false;
				//	}
				//}, 10*1000);
				//scheduled = true;
			}
		});
		client.setConnectionListener(new ConnectionListener(){
			@Override
			public void connected(InetAddress address) {}
			@Override
			public void disconnected(InetAddress a) {
				if (p == null) return;
				System.out.println("stop streaming");
				try {
					p = Runtime.getRuntime().exec("/usr/bin/pkill raspivid");
				} catch (IOException  e) {
                			e.printStackTrace();
                		}
				System.out.println("stoped streaming");
				p = null;
			}
		});
		(new Thread(client)).start();

		while(true){
			client.ask_streaming();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
