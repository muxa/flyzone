#include <jni.h>
#include <stdio.h>
#include "../detector_aprilzone_TagDetector36h11.h"

#include "apriltag.h"
#include "tag36h11.h"
#include "zarray.h"
#include "pnm.h"
#include "image_u8.h"

// least common multiple of 64 (sandy bridge cache line) and 24 (stride
// needed for RGB in 8-wide vector processing)
#define DEFAULT_ALIGNMENT 96

image_u8_t* image_u8_create_from_buffer(jbyte* im_buf) {
  pnm_t *pnm = (pnm_t*)calloc(1, sizeof(pnm_t));

  pnm->format = im_buf[1]-'0';
  assert(pnm->format == PNM_FORMAT_GRAY || pnm->format == PNM_FORMAT_RGB);

  int p = 2;
  
  int nparams = 0; // will be 3 when we're all done.
  int params[3];
  while (nparams < 3 && *(im_buf+p)!=0) {
    while (*(im_buf+p)==' ' || *(im_buf+p)==10)
      p++;
    
    int acc = 0;
    while (*(im_buf+p) >= '0' && *(im_buf+p) <= '9') {
      acc = acc*10 + *(im_buf+p) - '0';
      p++;
    }
    
    params[nparams] = acc;
    nparams++;
    p++;
  }

  assert(params[2] == 255);
  pnm->width = params[0];
  pnm->height = params[1];
  pnm->buflen = pnm->width * pnm->height;

  if (pnm->format == PNM_FORMAT_RGB)
    pnm->buflen *= 3;

  pnm->buf = (uint8_t*)malloc(pnm->buflen);
  memcpy(pnm->buf, im_buf+p, pnm->buflen);

  image_u8_t *im = image_u8_create_alignment(pnm->width, pnm->height, DEFAULT_ALIGNMENT);

  switch (pnm->format) {
    case PNM_FORMAT_GRAY: {
      for (int y = 0; y < im->height; y++)
	memcpy(&im->buf[y*im->stride], &pnm->buf[y*im->width], im->width);
      break;
    }
    case PNM_FORMAT_RGB: {
      // Gray conversion for RGB is gray = (r + g + g + b)/4
      for (int y = 0; y < im->height; y++) {
	for (int x = 0; x < im->width; x++) {
	  uint8_t gray = (pnm->buf[y*im->width*3 + 3*x+0] +    // r
			  pnm->buf[y*im->width*3 + 3*x+1] +    // g
			  pnm->buf[y*im->width*3 + 3*x+1] +    // g
			  pnm->buf[y*im->width*3 + 3*x+2])     // b
	    / 4;
	  im->buf[y*im->stride + x] = gray;
	}
      }
      break;
    }
  }

  pnm_destroy(pnm);
  return im;
}

apriltag_family_t* tf;
apriltag_detector_t *td;

JNIEXPORT void JNICALL Java_detector_aprilzone_TagDetector36h11_init (JNIEnv* env, jobject thisObj) {
  tf = tag36h11_create();
  td = apriltag_detector_create();

  td->debug = 0; // Enable debugging output (slow)
  td->nthreads = 4; // Use this many CPU threads
  td->quad_decimate = 2.0; // Decimate input image by this factor
  td->quad_sigma = 0.0; // Apply low-pass blur to input
  td->refine_edges = 0; // Spend more time trying to align edges of tags
  td->refine_decode = 0; // Spend more time trying to decode tags
  td->refine_pose = 0; // Spend more time trying to precisely localize tags
  
  apriltag_detector_add_family(td, tf);
}
 
JNIEXPORT jobjectArray JNICALL Java_detector_aprilzone_TagDetector36h11_detect (JNIEnv* env, jobject thisObj, jbyteArray frame) {
  // getting data from Java world
  jbyte* im_buf = env->GetByteArrayElements(frame, NULL);
  // create image_u8_t
  image_u8_t* im = image_u8_create_from_buffer(im_buf);
  // detecting
  zarray_t *detections = apriltag_detector_detect(td, im);
  // allocate memory for result
  jobjectArray result = env->NewObjectArray((jsize) zarray_size(detections),
					    env->FindClass("[D"), NULL);
  
  for (int i = 0; i < zarray_size(detections); i++) {
    apriltag_detection_t *det;
    zarray_get(detections, i, &det);
    // allocate memory for the tag's data:
    // id, (c)x, (c)y, H(omography)
    jdoubleArray id_x_y_H_p = env->NewDoubleArray(3+8+det->H->nrows*det->H->nrows);
    // collect the data
    jdouble id_x_y_p[] = {det->id,
			  det->c[0], det->c[1],
			  det->p[0][0], det->p[0][1],
			  det->p[1][0], det->p[1][1],
			  det->p[2][0], det->p[2][1],
			  det->p[3][0], det->p[3][1]};
    // write the data to the allocated array
    env->SetDoubleArrayRegion(id_x_y_H_p, 0, 3+8, id_x_y_p);
    env->SetDoubleArrayRegion(id_x_y_H_p, 3+8, det->H->nrows*det->H->nrows, det->H->data);
    // write the tag's data to the result
    env->SetObjectArrayElement(result, i, id_x_y_H_p);
  }
  // cleanup
  apriltag_detections_destroy(detections);
  image_u8_destroy(im);
  free(im_buf);
  
  return result;
}

JNIEXPORT void JNICALL Java_detector_aprilzone_TagDetector36h11_clean (JNIEnv* env, jobject thisObj) {
  apriltag_detector_destroy(td);
  tag36h11_destroy(tf);
}
