#include <jni.h>
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <raspicam/raspicam.h>

#include "../stream_player_RasPiCam.h"

using namespace std;

raspicam::RaspiCam* camera;
unsigned int WIDTH = 640;
unsigned int HEIGHT = 480;

int ROTATION = 270;

/*
 * Class:     RasPiCam
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_stream_player_RasPiCam_init(JNIEnv* env, jobject thisObj, jint width, jint height){
  WIDTH = width;
  HEIGHT = height;

  camera = new raspicam::RaspiCam();
  camera->setWidth(WIDTH);
  camera->setHeight(HEIGHT);
  camera->setRotation(ROTATION);
  camera->setAWB(raspicam::RASPICAM_AWB_TUNGSTEN);
  camera->setMetering(raspicam::RASPICAM_METERING_MATRIX);
  //Open camera 
  cout<<"Opening Camera..."<<endl;
  if (!camera->open()) {cerr<<"Error opening camera"<<endl;return;}
}

/*
 * Class:     RasPiCam
 * Method:    get_frame
 * Signature: ()[B
 */
JNIEXPORT jbyteArray JNICALL Java_stream_player_RasPiCam_capture (JNIEnv *env, jobject thisObj){
  //capture
  camera->grab();
  //allocate memory
  int size = camera->getImageTypeSize(raspicam::RASPICAM_FORMAT_RGB);
  unsigned char* data=new unsigned char[size];
  //extract the image in rgb format
  camera->retrieve (data, raspicam::RASPICAM_FORMAT_IGNORE);//get camera image
  // create header
  std::ostringstream sstream;
  sstream << "P6\n" << WIDTH << " " << HEIGHT << " 255\n";
  string str_header = sstream.str();
  const char* header = str_header.c_str();
  int h_size = str_header.length();
  //char* header = new char[15];
  //string header_str("P6\n640 480 255\n");
  //header_str.copy(header, 15);

  // allocate jbyte array
  jbyteArray result=env->NewByteArray(h_size+size);
  // copy header 
  env->SetByteArrayRegion(result, 0, h_size, (jbyte*)header);
  // copy data
  env->SetByteArrayRegion(result, h_size, size, reinterpret_cast<jbyte*>(data));
  // cleanup
  delete data;
  //delete[] header;
  return result;
}

/*
 * Class:     RasPiCam
 * Method:    clean
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_stream_player_RasPiCam_clean(JNIEnv* env, jobject thisObj) {
  camera->release();
  delete camera;
}
