package it.polimi.neslab.infrastructureFlyZone;

import java.util.List;

import com.codeminders.ardrone.ARDrone;

public class ARDrone2 implements DroneKeyboard, DroneWaypoints, DroneStepByStep, DroneInfo {

	ARDrone drone;

	public void reset() {
		try {
			drone.connect();
			drone.clearEmergencySignal();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void move(Coordinate coordinate) {
		
	}

	public void up() {
	}

	public void down() {
	}

	public void forward() {
	}

	public void backward() {
	}

	public void left() {
	}

	public void right() {
	}

	public List<Coordinate> getCheckpoints() {
		return null;
	}

	public void setLimit() {
	}

	public void delCheckpoint(Coordinate coordinate) {
	}

	public void addCheckpoint(Coordinate coordinate) {
	}

	public void runWaypoint() {
	}

	public void getVideo() {
	}

	public void getName() {
	}

	public void getSettings() {
	}

	public void getBattery() {
	}

}