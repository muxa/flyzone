package infrastructure;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.core.Scenario;

/**
 * Created by mike on 14/04/17.
 */
public class ExampleScenario9 extends Scenario {
    @Override
    public void run() {
        takeOff();

        hover(2000);
        move(new Coordinate(140d, 140d, 100d, 0d));
        //System.out.println("\n\n--------------------[FIRST TARGET REACHED\n\n");
        /*
        hover(2000);
        move(new Coordinate(13d, 7d, 70d, 45d));
*/
        hover(2000);

        System.err.println("Landing");
        land();
    }
}
