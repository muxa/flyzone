package infrastructure;

import it.polimi.neslab.infrastructure.testBed.*;

import java.net.UnknownHostException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by mike on 10/02/17.
 */
public class ExampleAutopilot {

    public static void main(String[] args) {

        // to remove log scripts
        Logger log = LogManager.getLogManager().getLogger("");
        for (Handler h : log.getHandlers()) {
            h.setLevel(Level.SEVERE);
        }

        // initialize connections
        TestBed.init();

        // bind drones to RPIs
        try {
            TestBed.addDrone("192.168.1.102", "192.168.1.6", Drone.Type.ARDrone);

            TestBed.addDrone("192.168.1.9", "192.168.1.3", Drone.Type.ARDrone);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        //TestBed2.setScenario("192.168.1.102", new ExampleScenario1022());
        TestBed.setScenario("192.168.1.102", new ExampleScenario102());
        TestBed.setScenario("192.168.1.9", new ExampleScenario9());

        // start executing the scenarios!
        TestBed.execute();
    }
}
