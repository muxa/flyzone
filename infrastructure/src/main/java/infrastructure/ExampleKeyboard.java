package infrastructure;

import it.polimi.neslab.infrastructure.testBed.Drone;
import org.jnativehook.keyboard.NativeKeyEvent;

import java.net.UnknownHostException;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by mike on 24/04/17.
 */
public class ExampleKeyboard {

    public static void main(String[] args) {


        // to remove log scripts
        Logger log = LogManager.getLogManager().getLogger("");
        for (Handler h : log.getHandlers()) {
            h.setLevel(Level.SEVERE);
        }

        // initialize connections
        TestBed.init();

        // bind drones to RPIs
        try {
            TestBed.addDrone("192.168.1.102", "192.168.1.6", Drone.Type.ARDrone, Drone.Control.Keyboard);
            TestBed.addDrone("192.168.1.9", "192.168.1.3", Drone.Type.ARDrone, Drone.Control.Keyboard);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        TestBed.bindKey("192.168.1.9", NativeKeyEvent.VC_1);
        TestBed.bindKey("192.168.1.102", NativeKeyEvent.VC_2);

        // start executing the scenarios!
        //TestBed.execute();
    }
}
