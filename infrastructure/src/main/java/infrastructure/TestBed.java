package infrastructure;

import it.polimi.neslab.flyzone.network.server.ConnectionListener;
import it.polimi.neslab.flyzone.network.server.CoordinatesListener;
import it.polimi.neslab.flyzone.network.server.UDPServer;
import it.polimi.neslab.infrastructure.testBed.*;
import it.polimi.neslab.infrastructure.testBed.core.*;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyListener;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mike on 10/02/17.
 */
public class TestBed {

    private static HashMap<String, DroneAutopilot> droneList = new HashMap<>();
    private static HashMap<String, String> drone2rpi = new HashMap<String, String>();
    private static List<Scenario> scenarios = new ArrayList<Scenario>();
    private static final UDPServer server = new UDPServer();
    private static final KeyListener _keyListener = new KeyListener();

    public static void init(){

        server.setConnectionListener(new ConnectionListener() {

            // When connection ok, initialize the right drone
            public void connected(final InetAddress address) {
                String rpi = address.getHostAddress();
                //System.out.println("connected " + rpi + " contains? "+droneList.containsKey(rpi));
                if (!droneList.containsKey(rpi)) return;
                System.out.println("connected " + rpi);
            }

            public void disconnected(InetAddress a) {
                String rpi = a.getHostAddress();
                if (!droneList.containsKey(rpi)) return;

                DroneAutopilot autopilot = droneList.get(rpi);
                if (!autopilot.isConnected()) return;
                try {
                    // stop the drone now!
                    autopilot.stop();
                    System.out
                            .println("I'm landing because I disconnected "
                                    + a.getHostAddress());
                    autopilot.disconnect();
                    Thread.sleep(4000);
                    autopilot.setDrone(null);
                    return;

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        server.setCoordinatesListener(new CoordinatesListener() {
            public void coordinatesUpdated(String ip, double x, double y,
                                           double a, long ts) {
                if (!droneList.containsKey(ip)) return;
                DroneAutopilot autopilot = droneList.get(ip);
                if (!autopilot.isConnected()) return;
                autopilot.updatePosition(x, y, a, ts);
            }
        });

        (new Thread(server)).start();

        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }

        GlobalScreen.addNativeKeyListener(_keyListener);
    }

    public static void addDrone(String droneIP, String rpiIP, Drone.Type type) throws UnknownHostException {
        addDrone(droneIP, rpiIP, type, Drone.Control.Autopilot);
    }

    public static void addDrone(String droneIP, String rpiIP, Drone.Type type, Drone.Control ctrlType) throws UnknownHostException {
        Drone drone = DroneFactory.buildDrone(InetAddress.getByName(droneIP), type);
        DroneAutopilot autopilot = AutopilotFactory.buildAutopilotFor(drone, ctrlType);

        autopilot.connect(server, InetAddress.getByName(rpiIP));
        droneList.put(rpiIP, autopilot);
        drone2rpi.put(droneIP, rpiIP);
/*
        if (ctrlType == Drone.Control.Keyboard) {
            Scenario sc = new KeyboardScenario();
            sc.setAutopilot(autopilot);
            scenarios.add(sc);
        }
        */
    }

    public static void setScenario(String droneIP, Scenario scenario) {
        DroneAutopilot autopilot = droneList.get(drone2rpi.get(droneIP));
        if (autopilot.getType() == Drone.Control.Keyboard) return;
        scenario.setAutopilot(autopilot);
        scenarios.add(scenario);
    }

    public static void execute(){
        for (Scenario scenario : scenarios)
            new Thread(scenario).start();
    }

    public static void bindKey(String droneIP, Integer key) {
        DroneAutopilot autopilot = droneList.get(drone2rpi.get(droneIP));
        if (autopilot.getType() != Drone.Control.Keyboard) return;
        _keyListener.addAutopilot(autopilot, key);
    }
}
