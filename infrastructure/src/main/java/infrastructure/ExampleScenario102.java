package infrastructure;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.core.Scenario;

/**
 * Created by mike on 13/04/17.
 */
public class ExampleScenario102 extends Scenario {

    @Override
    public void run() {

        takeOff();

        hover(2000);
        move(new Coordinate(263d, 303d, 100d, 0d));
        //System.out.println("\n\n--------------------[SECOND TARGET ROTATED\n\n");
/*
        hover(2000);
        move(new Coordinate(8d, 15d, 70d, -45d));
*/
        hover(2000);
        System.err.println("Landing");
        land();

    }

}
