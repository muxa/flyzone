package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.Drone;

/**
 * Created by mike on 10/02/17.
 */
public class NullAutopilot extends DroneAutopilot {
    @Override
    public void move(Coordinate c) {

    }

    @Override
    public void rotate(Double angle) {

    }

    @Override
    public void hover(long time) {

    }

    @Override
    public void hover() {

    }

    @Override
    public void handleCommand(int key) throws InterruptedException {

    }

    @Override
    public Drone.Control getType() {
        return null;
    }
}
