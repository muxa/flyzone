package it.polimi.neslab.infrastructure.testBed.ARDrone;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.Drone;
import it.polimi.neslab.infrastructure.testBed.core.DroneAutopilot;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;

/**
 * Created by mike on 24/04/17.
 */
public class ARDrone2Keypilot extends DroneAutopilot {
    @Override
    public void move(Coordinate c) {

    }

    @Override
    public void rotate(Double angle) {

    }

    @Override
    public void hover(long time) {
        try {
            _drone.getDrone().hover(time);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hover() {
        try {
            _drone.getDrone().hover();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Drone.Control getType() {
        return Drone.Control.Keyboard;
    }

    public void handleCommand(int key) throws InterruptedException {

        try {
            float speed = 0.2f;

            switch (key) {
                case NativeKeyEvent.VC_ENTER:
                    System.out.println("taking off...");
                    _drone.getDrone().takeOff();
                    break;
                case NativeKeyEvent.VC_ESCAPE:
                    System.out.println("disconnecting...");
                    _drone.getDrone().land();
                    Thread.sleep(2000);
                    _drone.getDrone().disconnect();
                    break;
                case NativeKeyEvent.VC_SPACE:
                    _drone.getDrone().land();
                    break;
                case NativeKeyEvent.VC_A:
                    _drone.getDrone().move(-speed, 0, 0, 0);

                    break;
                case NativeKeyEvent.VC_D:

                    _drone.getDrone().move(speed, 0, 0, 0);
                    break;
                case NativeKeyEvent.VC_W:
                    _drone.getDrone().move(0, -speed, 0, 0);
                    break;
                case NativeKeyEvent.VC_S:
                    _drone.getDrone().move(0, speed, 0, 0);
                    break;
                case NativeKeyEvent.VC_RIGHT:
                    _drone.getDrone().move(0, 0, 0, speed);
                    break;
                case NativeKeyEvent.VC_LEFT:
                    _drone.getDrone().move(0, 0, 0, -speed);
                    break;
                case NativeKeyEvent.VC_UP:
                    _drone.getDrone().move(0, 0, speed+(2/10*speed), 0);
                    break;
                case NativeKeyEvent.VC_DOWN:
                    _drone.getDrone().move(0, 0, -speed+(1/10*speed), 0);
                    break;
                case NativeKeyEvent.VC_E:
                    _drone.getDrone().clearEmergencySignal();
                    break;
                // case NativeKeyEvent.VK_PLUS:
                // drone.setSpeed(drone.getSpeed()+1);
                // break;
                // case NativeKeyEvent.VK_MINUS:
                // drone.setSpeed(drone.getSpeed()-1);
                // break;
            }
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
}
