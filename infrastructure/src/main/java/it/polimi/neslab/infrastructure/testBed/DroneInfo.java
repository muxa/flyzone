package it.polimi.neslab.infrastructure.testBed;

public interface DroneInfo {

  public void getVideo();

  public String getName();
  
  public void setName(String name);

  public int getBattery();

  public String getSettings();

}