package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.ARDrone.*;
import it.polimi.neslab.infrastructure.testBed.Drone;

/**
 * Created by mike on 10/02/17.
 */
public class AutopilotFactory {

    public static DroneAutopilot buildAutopilotFor(Drone drone, Drone.Control ctrlType) {
        switch (drone.getType()) {
            case ARDrone: return buildARDroneAutopilot(drone, ctrlType);
        }
        return new NullAutopilot();
    }

    private static DroneAutopilot buildARDroneAutopilot(Drone drone, Drone.Control ctrlType) {

        DroneAutopilot autopilot = null;
        NavStrategy navStrategy = null;

        switch (ctrlType) {
            case Autopilot:
                autopilot = new ARDrone2Autopilot();
                navStrategy = new ARDrone2NavStrategy();
                navStrategy.addNavStartegy("2D", new ARDrone22DSimpleStrategy());
                navStrategy.addNavStartegy("height", new ARDrone2HeightStartegy());
                break;
            case Keyboard:
                autopilot = new ARDrone2Keypilot();
                break;
            default:
                autopilot = new NullAutopilot();
                navStrategy = new NullStrategy();
        }

        autopilot.setNavStrategy(navStrategy);
        autopilot.setDrone(drone);
        //drone.activeNavData(server, address);
        return autopilot;
    }
}
