package it.polimi.neslab.infrastructure.testBed;

import java.util.List;

public interface DroneWaypoints {

  public void addCheckpoint(Coordinate coordinate);

  public void delCheckpoint(Coordinate coordinate);

  public List<Coordinate> getCheckpoints();

  public void runWaypoint();

}