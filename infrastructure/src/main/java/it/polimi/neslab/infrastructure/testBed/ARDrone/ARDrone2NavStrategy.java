package it.polimi.neslab.infrastructure.testBed.ARDrone;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.core.NavStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mike on 09/02/17.
 */
public class ARDrone2NavStrategy implements NavStrategy {

    private Map<String, NavStrategy> _strategies = new HashMap<>();

    @Override
    public void addNavStartegy(String key, NavStrategy strategy) {
        _strategies.put(key, strategy);
    }

    @Override
    public Double getSpeed(String key) {
        //System.out.println(this.getClass().getName()+" getting speed for "+key);
        String localKey = key.split("\\.")[0];

        if (_strategies.containsKey(localKey)) {
            //System.out.println(this.getClass().getName()+" getting speed for "+key.substring(localKey.length()));
            return _strategies.get(localKey).getSpeed(key.substring(localKey.length()));
        }
        return 0.0;
    }

    @Override
    public void reset() {
        for (NavStrategy strategy : _strategies.values()) strategy.reset();
    }

    @Override
    public void setDestination(Coordinate c) {
        for (NavStrategy strategy : _strategies.values()) strategy.setDestination(c);
    }

    @Override
    public void updateSpeedParameters(Coordinate position) {
        for (NavStrategy strategy : _strategies.values()) strategy.updateSpeedParameters(position);
        //System.out.println();
        //System.out.format(" x:%.5f\t y:%.5f\t z:%.5f\t a:%.5f%n", position.getX(), position.getY(), position.getZ(), position.getAlpha());
        //System.out.format("dx:%.5f\tdy:%.5f\tdz:%.5f\tda:%.5f%n", error_x, error_y, _errorZ, _errorA);
        //System.out.format("ss:%.5f\tsf:%.5f\tsz:%.5f\tsa:%.5f%n", speedSide, speedFront, speedZ, -speedA);

        //return new Double[]{speedSide, speedFront, speedZ, -speedA};
    }

    @Override
    public boolean isArrived() {
        for (Map.Entry<String, NavStrategy> entry : _strategies.entrySet()) {
            //System.out.println(entry.getKey()+" : "+entry.getValue().isArrived());
            if (!entry.getValue().isArrived()) return false;
        }
        return true;
    }
}
