package it.polimi.neslab.infrastructure.testBed;

import java.awt.BorderLayout;
import java.io.IOException;

import com.codeminders.ardrone.ARDrone;
import com.codeminders.ardrone.NavData;
import com.codeminders.ardrone.NavDataListener;
import com.codeminders.ardrone.ARDrone.VideoChannel;

public class VideoTest extends javax.swing.JFrame implements NavDataListener {
	private javax.swing.JPanel videoPanel;
	private final VideoPanel video = new VideoPanel();

	public VideoTest(ARDrone drone) {
		setAlwaysOnTop(true);
		initComponents();
		videoPanel.add(video);
		setLocationRelativeTo(null);
		setVisible(true);
        try {
			drone.selectVideoChannel(VideoChannel.HORIZONTAL_ONLY);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        video.setDrone(drone);
        drone.addNavDataListener(this);
	}

	private void initComponents() {
		this.setTitle("Video");
		this.setSize(320, 240);
		videoPanel = new javax.swing.JPanel();
		videoPanel.setBackground(new java.awt.Color(102, 102, 102));
		videoPanel.setPreferredSize(new java.awt.Dimension(960, 480));
		videoPanel.setLayout(new javax.swing.BoxLayout(videoPanel,
				javax.swing.BoxLayout.LINE_AXIS));
		this.add(videoPanel, BorderLayout.CENTER);
	}

	public void navDataReceived(NavData nd) {
		// TODO Auto-generated method stub
		
	}
}
