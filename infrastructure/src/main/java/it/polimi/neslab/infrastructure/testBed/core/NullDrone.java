package it.polimi.neslab.infrastructure.testBed.core;

import com.codeminders.ardrone.ARDrone;
import com.codeminders.ardrone.NavDataListener;
import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.Drone;
import org.jnativehook.keyboard.NativeKeyEvent;

import java.util.List;

/**
 * Created by mike on 10/02/17.
 */
public class NullDrone extends Drone {
    @Override
    public long getConnectTimeout() {
        return 0;
    }

    @Override
    public void move(Coordinate c) {

    }

    @Override
    public void rotate(Double angle) {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void takeOff() {

    }

    @Override
    public void land() {

    }

    @Override
    public void addCheckpoint(Coordinate coordinate) {

    }

    @Override
    public void delCheckpoint(Coordinate coordinate) {

    }

    @Override
    public List<Coordinate> getCheckpoints() {
        return null;
    }

    @Override
    public void runWaypoint() {

    }

    @Override
    public void getVideo() {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public int getBattery() {
        return 0;
    }

    @Override
    public void addNavDataListener(NavDataListener l) {

    }

    @Override
    public String getSettings() {
        return null;
    }

    @Override
    public ARDrone getDrone() {
        return null;
    }

    @Override
    public Type getType() {
        return null;
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {

    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

    }
}
