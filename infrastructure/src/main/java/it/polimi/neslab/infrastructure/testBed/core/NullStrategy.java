package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by mike on 09/02/17.
 */
public class NullStrategy implements NavStrategy {
    @Override
    public void addNavStartegy(String key, NavStrategy strategy) {
        throw new NotImplementedException();
    }

    @Override
    public Double getSpeed(String key) {
        throw new NotImplementedException();
    }

    @Override
    public void reset() {
        throw new NotImplementedException();
    }

    @Override
    public void setDestination(Coordinate c) {
        throw new NotImplementedException();
    }

    @Override
    public void updateSpeedParameters(Coordinate position) {
        throw new NotImplementedException();
    }

    @Override
    public boolean isArrived() {
        throw new NotImplementedException();
    }
}
