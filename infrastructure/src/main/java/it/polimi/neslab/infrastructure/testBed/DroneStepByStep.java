package it.polimi.neslab.infrastructure.testBed;

public interface DroneStepByStep {

	public void move(Coordinate coordinate);

}