package it.polimi.neslab.infrastructure.testBed;

public class PIDControl {
	private double dState;
	private double iState;
	private final double iMax;
	private final double iMin;

	private final double iGain;
	private final double pGain;
	private final double dGain;

	public PIDControl(double position, double iMax, double iMin, double iGain,
			double pGain, double dGain) {
		super();
		this.dState = position;
		this.iState = 0;
		this.iMax = iMax;
		this.iMin = iMin;
		this.iGain = iGain;
		this.pGain = pGain;
		this.dGain = dGain;
	}

	public double updatePID(double error, double position) {

		double pTerm = updateP(error);
		double iTerm = updateI(error);
		double dTerm = updateD(position);

		//System.out.println("pid:" + pTerm + ", " + iTerm + ", " + dTerm);
		return pTerm + iTerm - dTerm;
	}

	private double updateP(double error) {
		return pGain * error;
	}

	private double updateI(double error) {
		this.iState = iState + error;
		if (iState > iMax)
			iState = iMax;
		else if (iState < iMin)
			iState = iMin;
		return iGain * iState;
	}

	private double updateD(double position) {
		double d = dGain * (position - dState);
		dState = position;
		return d;
	}

}
