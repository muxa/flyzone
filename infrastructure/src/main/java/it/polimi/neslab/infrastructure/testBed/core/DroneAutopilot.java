package it.polimi.neslab.infrastructure.testBed.core;

import com.codeminders.ardrone.NavData;
import com.codeminders.ardrone.NavDataListener;
import it.polimi.neslab.flyzone.network.server.UDPServer;
import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.Drone;

import java.net.InetAddress;

/**
 * Created by mike on 10/02/17.
 */
public abstract class DroneAutopilot {

    protected Drone _drone;
    protected NavStrategy _strategy = new NullStrategy();
    private boolean _connected = false;
    private Coordinate _launchPosition = null;
    private boolean _isFlying = false;

    abstract public void move(Coordinate c);
    abstract public void rotate(Double angle);

    public Coordinate getPosition() {
        if (_drone == null) return null;
        return _drone.getPosition();
    }

    public void setDrone(Drone drone) {
        _drone = drone;
    }
    public Drone getDrone() {
        return _drone;
    }

    void setNavStrategy(NavStrategy s) {
        _strategy = s;
    }

    public void connect(final UDPServer server, final InetAddress address) {
        _drone.addNavDataListener(new NavDataListener() {
            public void navDataReceived(NavData nd) {
                // rpi need roll and pitch to be more precisely
                server.send(address, nd.getRoll(), nd.getPitch());
                getPosition().setZ(Float.valueOf(nd.getRawAltitude()).doubleValue());
                _drone.setBattery(nd.getBattery());
                _drone.setPitch(nd.getPitch());
                _drone.setRoll(nd.getRoll());
                _drone.setYaw(nd.getYaw());
            }
        });
        _connected = true;
    }

    public boolean isConnected() {
        return _connected;
    }

    public boolean isFlying() {
        return _isFlying;
    }

    public void disconnect() {
        _connected = false;
        _drone.disconnect();
    }

    public void stop() {
        _drone.stop();
    }

    public void updatePosition(double x, double y, double a, long ts) {
        //System.out.println("\n\n---\ngot: "+x+" "+y+" "+a+"---\n\n");

        if (_drone == null || _drone.getPosition() == null) return;

        _drone.getPosition().setX(x);
        _drone.getPosition().setY(y);
        _drone.getPosition().setAlpha(a);
    }

    public void takeOff() {
        _drone.takeOff();
        _isFlying = true;
        while (_drone.getPosition() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        _launchPosition = _drone.getPosition().clone();
    }

    public void land() {
        _drone.land();
        _isFlying = false;
        disconnect();
    }

    public void returnToLaunch() {
        move(_launchPosition);
    }

    public abstract void hover(long time);
    public abstract void hover();
    public abstract void handleCommand(int key) throws InterruptedException ;
    public abstract Drone.Control getType();
}
