package it.polimi.neslab.infrastructure.testBed;

import java.util.List;
import java.util.Scanner;

import com.codeminders.ardrone.NavDataListener;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import com.codeminders.ardrone.ARDrone;

public abstract class Drone implements DroneKeyboard, DroneStepByStep,
		DroneWaypoints, DroneInfo {

	public abstract long getConnectTimeout();

	public enum Type{
		ARDrone;
	}

	public enum Control{
		Keyboard,
		Autopilot;
	}

	private int battery;
	private float pitch, roll, yaw;

	protected volatile Coordinate position;

	public abstract void move(Coordinate c);

	// rotate drone to the specified orientation in degrees [-180:180]
	public abstract void rotate(Double angle);

	public abstract void disconnect();
	
	public abstract void stop();
	
	public abstract void takeOff();
	
	public abstract void land();
	
	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}
	
	public void keyboardCommand(){

		
		 try {
		 GlobalScreen.registerNativeHook();
		 } catch (NativeHookException ex) {
		 System.err
		 .println("There was a problem registering the native hook.");
		 System.err.println(ex.getMessage());
		
		 System.exit(1);
		 }
		
		 GlobalScreen.addNativeKeyListener(this);
	
		 }
	

	public abstract void addCheckpoint(Coordinate coordinate);

	public abstract void delCheckpoint(Coordinate coordinate);

	public abstract List<Coordinate> getCheckpoints();

	public abstract void runWaypoint();

	public abstract void getVideo();

	public abstract String getName();

	public abstract void setName(String name);

	public abstract String getSettings();

	// TODO: delete this method
	public abstract ARDrone getDrone();

	public abstract Drone.Type getType();

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}

	public void setBattery(int b) {
		battery = b;
	}

	public int getBattery() {
		return battery;
	}

	public abstract void addNavDataListener(NavDataListener l);
}