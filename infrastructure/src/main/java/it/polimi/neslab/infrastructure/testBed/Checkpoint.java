package it.polimi.neslab.infrastructure.testBed;

public abstract class Checkpoint {

  public Coordinate coordinate;

  public Integer time;

public Coordinate getCoordinate() {
	return coordinate;
}

public void setCoordinate(Coordinate coordinate) {
	this.coordinate = coordinate;
}

public Integer getTime() {
	return time;
}

public void setTime(Integer time) {
	this.time = time;
}

//  public Action action;
  
  

}