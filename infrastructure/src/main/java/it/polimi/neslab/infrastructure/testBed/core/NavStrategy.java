package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.Coordinate;

/**
 * Created by mike on 09/02/17.
 */
public interface NavStrategy {

    void addNavStartegy(String key, NavStrategy strategy);

    Double getSpeed(String key);

    void reset();

    void setDestination(Coordinate c);

    void updateSpeedParameters(Coordinate position);

    boolean isArrived();
}
