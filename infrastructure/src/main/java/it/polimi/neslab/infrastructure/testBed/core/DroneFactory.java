package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.ARDrone.ARDrone2;
import it.polimi.neslab.infrastructure.testBed.Drone;

import java.net.InetAddress;

/**
 * Created by mike on 10/02/17.
 */
public class DroneFactory {

    public static Drone buildDrone(InetAddress ipDrone, Drone.Type type) {
        switch (type) {
            case ARDrone: return new ARDrone2(ipDrone, 3000, 3000);
        }
        return new NullDrone();
    }
}
