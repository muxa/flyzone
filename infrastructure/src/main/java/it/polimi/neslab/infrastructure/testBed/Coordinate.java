package it.polimi.neslab.infrastructure.testBed;

public class Coordinate {

	private Long ts;
	private Double y;

	private Double x;

	private Double z;

	private Double alpha;

	public Coordinate(Double x, Double y, Double z, Double alpha, Long ts) {
		this.y = y;
		this.x = x;
		this.z = z;
		this.alpha = alpha;
		this.ts = ts;
	}
	
	public Coordinate(Double x, Double y, Double z, Double alpha) {
		this.y = y;
		this.x = x;
		this.z = z;
		this.alpha = alpha;
	}

	public Coordinate(Double x, Double y, Double z) {
		this.y = y;
		this.x = x;
		this.z = z;
		this.alpha = null;
	}

	public Coordinate() {

	}

	public Coordinate(double[] state) {
		if (state.length != 3) return;
		this.x = state[0];
		this.y = state[1];
		this.alpha = state[2];
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getZ() {
		return z;
	}

	public void setZ(Double z) {
		this.z = z;
	}

	public Double getAlpha() {
		return alpha;
	}

	public void setAlpha(Double alpha) {
		this.alpha = alpha;
	}

	public Long getTimestamp() {
		return ts;
	}

	public String toString() {
		return "x:"+x+"\ty:"+y+"\tz:"+z+"\ta:"+alpha;
	}

	public Coordinate clone() {
		return new Coordinate(x, y, z, alpha, ts);
	}

}