package it.polimi.neslab.infrastructure.testBed.ARDrone;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.core.NavStrategy;
import it.polimi.neslab.infrastructure.testBed.core.PIDControl2;

/**
 * Created by mike on 09/02/17.
 */
public class ARDrone2HeightStartegy implements NavStrategy {
    private PIDControl2 _heightPID;
    private Double _destZ;
    private double _minZError;
    private double _errorZ;
    private Double _heightSpeed = 0.0;

    @Override
    public void addNavStartegy(String key, NavStrategy strategy) {

    }

    @Override
    public Double getSpeed(String key) {
        //System.out.println(this.getClass().getName()+" getting speed for "+key);
        //System.out.println(_heightSpeed);
        return _heightSpeed;
    }

    @Override
    public void reset() {
        _heightPID = new PIDControl2(0.00075, 0.0, 0.0);
    }

    @Override
    public void setDestination(Coordinate c) {
        reset();
        _destZ = c.getZ();
        _minZError = 20.0;
    }

    @Override
    public void updateSpeedParameters(Coordinate position) {
        if (_destZ == null) return;
        _errorZ = _destZ - position.getZ();


        _heightSpeed = _heightPID.updatePID(_errorZ, position.getZ());
    }

    @Override
    public boolean isArrived() {
        return Math.abs(_errorZ) <= _minZError;
    }
}
