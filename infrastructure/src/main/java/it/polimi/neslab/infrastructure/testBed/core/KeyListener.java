package it.polimi.neslab.infrastructure.testBed.core;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by mike on 28/04/17.
 */
public class KeyListener implements NativeKeyListener {
    private HashMap<Integer, DroneAutopilot> _autopilots = new HashMap<>();
    private Integer _id = null;

    public void addAutopilot(DroneAutopilot autopilot, Integer id) {
        System.out.println("bound to: "+id);
        _autopilots.put(id, autopilot);
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        int key = nativeKeyEvent.getKeyCode();
        System.out.println("pressed: "+key+" in list?: "+_autopilots.containsKey(key));
        if (_autopilots.containsKey(key)) _id = key;
        System.out.println("ID: "+_id);
        if (_id == null) {
            for (DroneAutopilot autopilot : _autopilots.values()) {
                try {
                    autopilot.handleCommand(key);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        try {
            _autopilots.get(_id).handleCommand(key);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        for (DroneAutopilot autopilot : _autopilots.values()) {
            autopilot.hover();
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent){

    }
}
