package it.polimi.neslab.infrastructure.testBed.ARDrone;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.Drone;
import it.polimi.neslab.infrastructure.testBed.core.DroneAutopilot;
import it.polimi.neslab.infrastructure.testBed.PIDControl;

import java.io.IOException;

/**
 * Created by mike on 10/02/17.
 */
public class ARDrone2Autopilot extends DroneAutopilot {

    @Override
    public void move(Coordinate c) {
        //operate(new Coordinate(c.getX(), c.getY(), c.getZ(),
        //        getPosition().getAlpha()), false);
        //rotate(c.getAlpha());
        operate(c, true);
    }

    public void operate(Coordinate c, boolean rotationOnly) {
        //if (rotationOnly)
        _strategy.setDestination(c);
        //else _strategy.setDestination(new Coordinate(c.getX(), c.getY(), c.getZ(), null));

        System.err.println("["+_drone.getDrone().getDrone_addr().getHostAddress()+"]"+ _drone.getBattery());
        System.err.println("["+_drone.getDrone().getDrone_addr().getHostAddress()+"]"+"MOVE...");

        while (getPosition() == null) {
            try {

                System.err.println("["+_drone.getDrone().getDrone_addr().getHostAddress()+"]"+"Waiting for first coordinate!");
                _drone.getDrone().hover(2000);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        System.err.println("["+_drone.getDrone().getDrone_addr().getHostAddress()+"]"+"CHECKED READY...");
        long start = System.currentTimeMillis();
        while (_drone.getPosition().getX() == null || _drone.getPosition().getY() == null) {
            //System.err.println("["+_drone.getDrone().getDrone_addr().getHostAddress()+"]"+"wait for position");
            if (System.currentTimeMillis() - start > _drone.getConnectTimeout()) {
                land();
                return;
            }
        }
        //while (_motionModel == null || _motionModel.getPredictedState() == null){}
        //while (_motionModel2 == null || _motionModel2.getPredictedState() == null){}

        Coordinate oldPos = null;

        do {
            if (_drone.getPosition().getZ() < 60)
                move2altitude(60.0f);




            //_strategy.updatePosition(position);
            //System.out.println("\nStep");
            //Coordinate state = _motionModel.getPredictedState();
            //System.out.println("Estimated: "+state);
            //Coordinate state2 = _motionModel2.getPredictedState();
            //System.out.println("Estimated2: "+state2);
            Coordinate pos = _drone.getPosition().clone();


            long t = System.currentTimeMillis();

            System.out.println("Measured: "+pos);
            //state2.setZ(pos.getZ());
            _strategy.updateSpeedParameters(pos);//_drone.getPosition());

            Double _frontBackSpeed = _strategy.getSpeed("2D.frontBack");
            Double _leftRightSpeed = _strategy.getSpeed("2D.leftRight");
            Double _heightSpeed = _strategy.getSpeed("height");
            Double _orientationSpeed = _strategy.getSpeed("2D.orientation");

            System.out.println(_frontBackSpeed + " " + _leftRightSpeed + " ");

            try {
                //if (!rotationOnly) {
                    if (_frontBackSpeed.doubleValue() == 0.0 && _leftRightSpeed.doubleValue() == 0.0 && _orientationSpeed.floatValue() == 0.0) {
                        _drone.getDrone().hover(30);
                    } else {
                        _drone.getDrone().move(
                                _leftRightSpeed.floatValue(),
                                _frontBackSpeed.floatValue(),
                                _heightSpeed.floatValue(),
                                0);//, 100 - (System.currentTimeMillis()-t));
                        long elapced = System.currentTimeMillis() - t;
                        if (elapced < 20) Thread.sleep(20 - elapced);

                        _drone.getDrone().move(0, 0, 0, _orientationSpeed.floatValue());
                        elapced = System.currentTimeMillis() - t;
                        if (elapced < 30) Thread.sleep(30 - elapced);

                    }
                /*}else {
                    if (_frontBackSpeed.doubleValue() == 0.0 && _leftRightSpeed.doubleValue() == 0.0 && _orientationSpeed == 0.0) {
                        _drone.getDrone().hover(33);
                    } else {
                        _drone.getDrone().move(_leftRightSpeed.floatValue(),
                                _frontBackSpeed.floatValue(),
                                _heightSpeed.floatValue(), _orientationSpeed.floatValue());

                        long elapced = System.currentTimeMillis() - t;
                        if (elapced < 30) Thread.sleep(30 - elapced);
                    }
                }*/

                    //_drone.getDrone().move(0, 0, 0, 0);//_orientationSpeed.floatValue());//, 100 - (System.currentTimeMillis()-t));
                    //elapced = System.currentTimeMillis() - t;
                    //if (elapced < 100) Thread.sleep(100 - elapced);

                //_drone.getDrone().move(0, 0, 0, 0);
                //_drone.getDrone().hover(50);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            oldPos = pos.clone();
            //_strategy.updatePosition(position);
            System.out.println(_strategy.isArrived());
        } while (!_strategy.isArrived());
        // fly a little to stabilize

        try {
            _drone.getDrone().move(0, 0, 0, 0, 100);
            _drone.getDrone().hover(200);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void rotate(Double angle) {
        operate(new Coordinate(
                getPosition().getX(),
                getPosition().getY(),
                getPosition().getZ(),
                angle), true);
    }

    @Override
    public void hover(long time) {
        try {
            _drone.getDrone().hover(time);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hover() {
        try {
            _drone.getDrone().hover();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleCommand(int key) throws InterruptedException {

    }

    @Override
    public Drone.Control getType() {
        return Drone.Control.Autopilot;
    }

    private void move2altitude(float altitude) {
        float minError = 10f;
        float speedZ;
        float speedOld = 0;
        PIDControl pidZ;


        pidZ = new PIDControl(_drone.getPosition().getZ(), 8, 12, 0.008, 0.01, 0.02);

        //pidZ = new PIDControl2(0.005, 0.0, 0.0);
        float error;
        error = (float) (altitude - _drone.getPosition().getZ());

        while (Math.abs(error) > minError) {
            long t = System.currentTimeMillis();
            speedZ = (float) pidZ.updatePID(error, _drone.getPosition().getZ());


            try {
                if (speedZ > 1.0f)
                    speedZ = 1.0f;
                _drone.getDrone().move(0, 0, speedZ, 0);
                long elapced = System.currentTimeMillis()-t;
                if (elapced < 100) Thread.sleep(100-elapced);
                //drone.hover(100);

                // System.out.println("I'm working for you...");
                //System.err.println(position.getZ());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            error = (float) (altitude - _drone.getPosition().getZ());
            speedOld = speedZ;

        }
    }
}
