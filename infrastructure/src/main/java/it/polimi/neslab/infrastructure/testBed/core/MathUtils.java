package it.polimi.neslab.infrastructure.testBed.core;

import org.jblas.DoubleMatrix;
import org.jblas.Singular;

/**
 * Created by mike on 22/02/17.
 */
public class MathUtils {
    public static Double wrapInCircle(Double a) {
        //System.out.format("errorA: %.5f = %.5f - %.5f%n", _errorA, _destA, a);
        // if difference is greater than 360 (lower than -360 to the right), then it does not make sense to make a full rotation
        // and we cut the full circle.
        a = a < -360 ? a%360 : a > 360 ? a%360 : a;
        //System.out.format("errorA: %.5f%n", _errorA);
        // if difference is greater than 180 (lower than -180 to the right), then it does not make sense to make
        // a half-circle rotation, instead we just rotate to the opposite side.
        a = a < -180 ? a%180 + 180 : a > 180 ? a%180 - 180 : a;

        return a;
    }

    public static DoubleMatrix inverse3x3(DoubleMatrix m) {
        if (m.columns != 3 || m.rows != 3) return m;
        DoubleMatrix[] usv = Singular.fullSVD(m);
        for (int i = 0; i < m.columns; i++) {
            usv[1].put(i, 1.0/usv[1].get(i));
        }
        return usv[2].mmul(DoubleMatrix.diag(usv[1])).mmul(usv[0].transpose());
    }

    public static double motionEquation(double[] x, long[] t, double dt) {
        if (x.length == 0 || x.length != t.length) return 0.0;
        if (x.length == 1) return x[0];
        if (x.length == 2) {
            double v_x = (x[1]-x[0])*1.0/(t[1]-t[0]);
            return x[1] + v_x*dt;
        }
        if (x.length == 3) {
            double v_x_1 = (x[1]-x[0])*1.0/(t[1]-t[0]);
            double v_x_2 = (x[2]-x[1])*1.0/(t[2]-t[1]);
            double a_x = (v_x_2-v_x_1)*1.0/(t[2]-t[1]);
            return x[2] + v_x_2*dt;// + 0.5*a_x*dt*dt;
        }
        return 0.0;
    }
}
