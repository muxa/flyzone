package it.polimi.neslab.infrastructure.testBed.ARDrone;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import it.polimi.neslab.flyzone.network.server.UDPServer;
import it.polimi.neslab.infrastructure.testBed.*;
import it.polimi.neslab.infrastructure.testBed.core.NavStrategy;

import org.jnativehook.keyboard.NativeKeyEvent;

import com.codeminders.ardrone.ARDrone;
import com.codeminders.ardrone.NavData;
import com.codeminders.ardrone.NavDataListener;

public class ARDrone2 extends Drone {

	private ARDrone drone;
	private boolean rpiPresence;
	private int battery;
	private String setting;
	private float pitch, roll, yaw;
	private NavStrategy _strategy = new ARDrone2NavStrategy();//new NullStrategy();

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}

	private String name;
	private float maxSpeed = 1.0f;
	private List<Coordinate> checkpoints;

	static final long CONNECT_TIMEOUT = 3000;

	static private final String[] US_FREQ = new String[]{"7","8"};
	static private int USF_COUNT = 0;

	static private String getUniqueUSFrequency(){
		if (USF_COUNT >= 2) USF_COUNT = 0;
		return US_FREQ[USF_COUNT++];
	}

	// when constructor is called, it creates a ARDrone object and wait until
	// the drone is ready
	public ARDrone2(InetAddress address, int i, int j) {
		try {
			// Create ARDrone object,
			// connect to drone and initialize it and start nav data listener
			rpiPresence = true;
			drone = new ARDrone(address, i, j);
			name = "ARDRONE " + drone.getDroneVersion();
			drone.connect();
			drone.clearEmergencySignal();
			drone.trim();
			position = new Coordinate();
			// Wait until drone is ready
			drone.waitForReady(CONNECT_TIMEOUT);
			drone.sendAllNavigationData();
			drone.disableAutomaticVideoBitrate();
			drone.disableVideo();
			drone.setConfigOption(ARDrone.ConfigOption.FLIGHT_WITHOUT_SHELL, "1");
			drone.setConfigOption(ARDrone.ConfigOption.OUTDOOR, "1");
			// we choose the next out of the available ultrasonic frequences
			// it is done to avoid US interference between the drones
			drone.setConfigOption("pic:ultrasound_freq", ARDrone2.getUniqueUSFrequency());
			drone.clearEmergencySignal();
			drone.trim();

		} catch (Throwable e) {
			e.printStackTrace();
		}
		checkpoints = new ArrayList<Coordinate>();
	}

	public void disconnect() {
		try {
			Thread.sleep(2000);
			drone.disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reset() {

		try {

			drone.clearEmergencySignal();

		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public Coordinate getPosition() {
		// if not new coordinate for at least 3 seconds then land
		if (position != null && position.getTimestamp() != null &&
				System.currentTimeMillis() - position.getTimestamp() > ARDrone2.CONNECT_TIMEOUT) {
			System.err.println("[" + this.drone.getDrone_addr().getHostAddress() + "]" + "DISCONNECTED! Landing...");
			land();
		}
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}

	public void takeOff() {
		try {
			drone.trim();
			Thread.sleep(500);
			// Take off
			drone.takeOff();
			Thread.sleep(5000);
			drone.hover(1000);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void land() {
		try {
			drone.hover(1000);
			drone.land();
			// Give it some time to land
			Thread.sleep(5000);
			drone.trim();
			Thread.sleep(1000);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void activeNavData(final UDPServer server, final InetAddress address) {
		drone.addNavDataListener(new NavDataListener() {
			public void navDataReceived(NavData nd) {
				// rpi need roll and pitch to be more precisely
				server.send(address, nd.getRoll(), nd.getPitch());
				getPosition().setZ(Float.valueOf(nd.getRawAltitude()).doubleValue());
				setBattery(nd.getBattery());
				setPitch(nd.getPitch());
				setRoll(nd.getRoll());
				setYaw(nd.getYaw());
			}
		});
	}

	public void stop() {
		try {
			drone.hover(1000);
			drone.playAnimation(0, 1000);
			drone.land();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void checkReady() {
		//if (getBattery() < 30) {
		//	stop();
		//	System.err.println("CHARGE ME!");
		//}
		while (getPosition() == null) {
			try {

				System.err.println("["+this.drone.getDrone_addr().getHostAddress()+"]"+"Waiting for first coordinate!");
				drone.hover(2000);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/***************** DRONE STEP BY STEP **************/


	public void move2altitude(float altitude) {
		float minError = 10f;
		float speedZ;
		float speedOld = 0;
		PIDControl pidZ;

		checkReady();

		if (rpiPresence == false)
			pidZ = new PIDControl(position.getZ(), 8, 12, 0.008, 0.008, 0.03);
		else
			pidZ = new PIDControl(position.getZ(), 8, 12, 0.008, 0.01, 0.02);

		//pidZ = new PIDControl2(0.005, 0.0, 0.0);
		float error;
		error = (float) (altitude - position.getZ());

		while (Math.abs(error) > minError) {
			long t = System.currentTimeMillis();
			speedZ = (float) pidZ.updatePID(error, position.getZ());


			try {
				if (speedZ > maxSpeed)
					speedZ = maxSpeed;
				drone.move(0, 0, speedZ, 0);
				long elapced = System.currentTimeMillis()-t;
				if (elapced < 100) Thread.sleep(100-elapced);
				//drone.hover(100);

				// System.out.println("I'm working for you...");
				//System.err.println(position.getZ());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			error = (float) (altitude - position.getZ());
			speedOld = speedZ;

		}
	}

	public void setStrategy(NavStrategy strategy) {
		_strategy = strategy;
	}

	@Override
	public long getConnectTimeout() {
		return CONNECT_TIMEOUT;
	}

	public void move(Coordinate c) {

		_strategy.setDestination(c);

		System.err.println("["+this.drone.getDrone_addr().getHostAddress()+"]"+"MOVE...");
		checkReady();
		System.err.println("["+this.drone.getDrone_addr().getHostAddress()+"]"+"CHECKED READY...");
		while (position.getX() == null || position.getY() == null) {
			//System.err.println("["+this.drone.getDrone_addr().getHostAddress()+"]"+"wait for position");
		}

		do {
			if (position.getZ() < 50)
				move2altitude(50.0f);

			long t = System.currentTimeMillis();

			//_strategy.updatePosition(position);

			_strategy.updateSpeedParameters(position);

			Double _frontBackSpeed = _strategy.getSpeed("2D.frontBack");
			Double _leftRightSpeed = _strategy.getSpeed("2D.leftRight");
			Double _heightSpeed = _strategy.getSpeed("height");
			Double _orientationSpeed = _strategy.getSpeed("orientation");

			try {
				//drone.move(speedSide, speedFront, speedZ, 0
					drone.move(_leftRightSpeed.floatValue(), _frontBackSpeed.floatValue(), _heightSpeed.floatValue(), 0);//, 100 - (System.currentTimeMillis()-t));
				    long elapced = System.currentTimeMillis()-t;
					if (elapced < 100) Thread.sleep(100-elapced);
					drone.move(0, 0, 0, _orientationSpeed.floatValue());//, 100 - (System.currentTimeMillis()-t));
					elapced = System.currentTimeMillis()-t;
					if (elapced < 150) Thread.sleep(150-elapced);
					drone.move(0, 0, 0, 0);
					drone.hover(50);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//_strategy.updatePosition(position);
		} while (!_strategy.isArrived());
		// fly a little to stabilize

		try {
			drone.move(0, 0, 0, 0, 100);
			drone.hover(200);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void rotate(Double angle) {
		move(new Coordinate(null, null, null, angle));
	}

	/**************** DRONE WAYPOINTS *****************/

	public void delCheckpoint(Coordinate coordinate) {
		checkpoints.remove(coordinate);
	}

	public void addCheckpoint(Coordinate coordinate) {
		checkpoints.add(coordinate);
	}

	public void runWaypoint() {
		for (Coordinate c : checkpoints) {
			move(c);
			try {
				drone.hover(2000);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public List<Coordinate> getCheckpoints() {
		return checkpoints;
	}

	/***************** DRONE INFO *********************/

	public void getVideo() {
		try {
			drone.enableVideo();
			drone.enableAutomaticVideoBitrate();
			drone.startVideo();
			VideoTest dr = new VideoTest(drone);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSettings() {
		return setting;
	}

	public ARDrone getDrone() {
		return drone;
	}

	@Override
	public Drone.Type getType() {
		return Type.ARDrone;
	}

	public void setDrone(ARDrone drone) {
		this.drone = drone;
	}

	public void setBattery(int b) {
		battery = b;
	}

	public int getBattery() {
		return battery;
	}

	@Override
	public void addNavDataListener(NavDataListener l) {
		drone.addNavDataListener(l);
	}

	public void setSetting(String s) {
		this.setting = s;
	}

	public void setSpeedLimit(float speed) {
		this.maxSpeed = speed;
	}

	/****************** KEYBOARD INTERFACE *******************/

	public void nativeKeyPressed(NativeKeyEvent e) {

		int key = e.getKeyCode();

		try {
			handleCommand(key);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void nativeKeyReleased(NativeKeyEvent e) {
		// System.out.println("Key released: " + e.getKeyChar());

		try {
			drone.hover();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected void handleCommand(int key) throws InterruptedException {
		// just for debugging
		// if (key > 0)
		// {
		// System.out.println("KeyboardCommandManager: Keyboard input is disabled");
		// return;
		// }

		try {
			float speed = 0.2f;
			
			switch (key) {
			case NativeKeyEvent.VC_ENTER:
				System.out.println("taking off...");
				drone.takeOff();
				break;
			case NativeKeyEvent.VC_ESCAPE:
				System.out.println("disconnecting...");
				drone.land();
				Thread.sleep(2000);
				drone.disconnect();
				break;
			case NativeKeyEvent.VC_SPACE:
				drone.land();
				break;
			case NativeKeyEvent.VC_A:
				drone.move(-speed, 0, 0, 0);

				break;
			case NativeKeyEvent.VC_D:

				drone.move(speed, 0, 0, 0);
				break;
			case NativeKeyEvent.VC_W:
				drone.move(0, -speed, 0, 0);
				break;
			case NativeKeyEvent.VC_S:
				drone.move(0, speed, 0, 0);
				break;
			case NativeKeyEvent.VC_RIGHT:
				drone.move(0, 0, 0, speed);
				break;
			case NativeKeyEvent.VC_LEFT:
				drone.move(0, 0, 0, -speed);
				break;
			case NativeKeyEvent.VC_UP:
				drone.move(0, 0, speed+(2/10*speed), 0);
				break;
			case NativeKeyEvent.VC_DOWN:
				drone.move(0, 0, -speed+(1/10*speed), 0);
				break;
			case NativeKeyEvent.VC_E:
				drone.clearEmergencySignal();
				break;
			// case NativeKeyEvent.VK_PLUS:
			// drone.setSpeed(drone.getSpeed()+1);
			// break;
			// case NativeKeyEvent.VK_MINUS:
			// drone.setSpeed(drone.getSpeed()-1);
			// break;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void nativeKeyTyped(NativeKeyEvent e) {
		// TODO leave empty
	}

}
