package it.polimi.neslab.infrastructure.testBed.core;

import it.polimi.neslab.infrastructure.testBed.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 10/02/17.
 */
public abstract class Scenario implements Runnable {

    private List<Coordinate> _waypoints = new ArrayList<>();

    protected DroneAutopilot _autopilot = new NullAutopilot();
    private volatile boolean _suspend;
    private volatile boolean _cancel;

    public void setAutopilot(DroneAutopilot a) {
        _autopilot = a;
    }

    public synchronized void move(Coordinate c) {
        // convert (x, y) coordinates from cantimeters
        _autopilot.move(new Coordinate(c.getX()/20.2, c.getY()/20.2, c.getZ(), c.getAlpha()));
    }

    public synchronized void rotateTo(Double angle) {
        _autopilot.rotate(angle);
    }

    public synchronized void takeOff() {
        _autopilot.takeOff();
    }

    public synchronized void land() {
        _autopilot.land();
    }

    public synchronized void returnToLaunch(){
        _autopilot.returnToLaunch();
    }

    public synchronized void addWaypoint(Coordinate c) {
        // convert (x, y) coordinates from cantimeters
        _waypoints.add(new Coordinate(c.getX()/20.2, c.getY()/20.2, c.getZ(), c.getAlpha()));
    }

    public synchronized void removeWaypoint(Coordinate c) {
        // convert (x, y) coordinates from cantimeters
        _waypoints.remove(new Coordinate(c.getX()/20.2, c.getY()/20.2, c.getZ(), c.getAlpha()));
    }

    public synchronized List<Coordinate> getWaypoints() {
        return _waypoints;
    }

    public synchronized void runWaypoints() {
        if (!_autopilot.isFlying()) takeOff();
        for (Coordinate c : _waypoints) {
            while (_suspend) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (_cancel) {
                returnToLaunch();
                return;
            }
            _autopilot.move(c);
        }
    }

    public synchronized void suspendWaypoints() {
        _suspend = true;
    }

    public synchronized void cancelAndReturn() {
        _cancel = true;
    }

    public void hover(long time) {
        _autopilot.hover(time);
    }
}
