package it.polimi.neslab.infrastructure.testBed.ARDrone;

import it.polimi.neslab.infrastructure.testBed.Coordinate;
import it.polimi.neslab.infrastructure.testBed.core.NavStrategy;

/**
 * Created by mike on 24/03/17.
 */
public class ARDrone22DSimpleStrategy implements NavStrategy {

    private Double _destX;
    private Double _destY;

    private double _minXYError;

    private Double _oldX = 0.0;
    private Double _oldY = 0.0;
    private long _oldT = 0;

    private Double _errorXY = 100.0;
    private Double _frontBackTilt = 0.0;
    private Double _leftRightTilt = 0.0;

    private Double _maxRoll = 50.0;
    private double _maxAccY = Math.tan(_maxRoll*Math.PI/180)*9.8;
    private Double _maxPitch = 50.0;
    private double _maxAccX = Math.tan(_maxPitch*Math.PI/180)*9.8;
    private Double _maxVelX = 2.0;
    private Double _maxVelY = 2.0;
    private long _t;

    double _error_x, _error_y;
    double _velX, _velY;
    double _accX, _accY;
    //Double _estX, _estY;
    private boolean _estimated;
    private double _nearDistance;

    private Double _destA;
    private Double _minAError;
    private double _errorA;
    private Double _oldA = 0.0;
    private Double _orientationSpeed = 0.0;
    private double _velA;
    private Double _maxOrientationSpeed = 0.2;

    @Override
    public void addNavStartegy(String key, NavStrategy strategy) {

    }

    @Override
    public Double getSpeed(String key) {
        //System.out.println(this.getClass().getName()+" getting speed for "+key);
        String localKey = key.split("\\.")[1];
        //System.out.println(this.getClass().getName()+" getting speed for "+localKey);
        if ("frontBack".equals(localKey)) {
            //System.out.println(_frontBackTilt);
            return _frontBackTilt*3;
        }
        if ("leftRight".equals(localKey)) {
            //System.out.println(_frontBackTilt);
            return _leftRightTilt*3;
        }
        if ("orientation".equals(localKey)) {
            //System.out.println(_frontBackTilt);
            return -_orientationSpeed;
        }
        return 0.0;
    }

    @Override
    public void reset() {

        _oldX = 0.0;
        _oldY = 0.0;
        _oldT = 0;
        _oldA = 0.0;
        _estimated = true;

        _error_x = 0;
        _error_y = 0;
        _velX = 0;
        _velY = 0;
        _velA = 0;
        _accX = 0;
        _accY = 0;

        _errorXY = 100.0;

    }

    @Override
    public void setDestination(Coordinate c) {
        reset();

        _destX = c.getX();
        _destY = c.getY();

        _oldX = null;
        _oldY = null;

        _minXYError = 1.5;
        _nearDistance = 3.0;

        _destA = c.getAlpha();

        _minAError = 10.0;
    }

    @Override
    public void updateSpeedParameters(Coordinate position) {
        // get a timestamp to calculate time difference dt
        long t = System.currentTimeMillis();
        long dt = t - _t;
        //System.out.println("dt: " + dt);
        _t = t;
        // if no destination set then exit
        if (_destX == null || _destY == null) return;

        if (_oldX == null || _oldY == null) {
            _oldX = position.getX();
            _oldY = position.getY();
            return;
        }

        // work in 2D plane
        double p_x = position.getX();
        double p_y = position.getY();
        double a = position.getAlpha();


        if (_oldX.equals(p_x) && _oldY.equals(p_y)) {
            //System.out.println("---------ESTIMATED---------------------");
            // if the location is not updated, we estimate error based on the previous acceleration and velocity
            _velX = _velX + _accX*dt*1.0/1000;
            _velY = _velY + _accY*dt*1.0/1000;

            double da = _velA*dt*1.0/1000;
            //System.out.println("da: "+da);

            _errorA = _errorA - da;

            _error_x = _error_x - _velX*dt*1.0/1000;
            _error_y = _error_y - _velY*dt*1.0/1000;

            _error_x = _error_x * Math.cos(da * Math.PI / 180) - _error_y * Math.sin(da * Math.PI / 180);
            _error_y = _error_x * Math.sin(da * Math.PI / 180) + _error_y * Math.cos(da * Math.PI / 180);

            _estimated = true;
        } else {
            //System.out.println("---------CALCULATED---------------------");
            // calculate error based on the received update
            // 1. shift the frame of reference to the drone's position
            double x_prime = _destX - p_x;
            double y_prime = _destY - p_y;

            // 2. rotate the frame of reference according to the drone's orientation
            _error_x = x_prime * Math.cos(a * Math.PI / 180) - y_prime * Math.sin(a * Math.PI / 180);
            _error_y = x_prime * Math.sin(a * Math.PI / 180) + y_prime * Math.cos(a * Math.PI / 180);

            // calculate time difference from the last update
            long cdt = t - _oldT;
            //System.out.println("cdt: " + cdt);
            _velX = cdt == 0 ? 0 : (p_x - _oldX) * 1000 * 0.2 / cdt;
            _velY = cdt == 0 ? 0 : (p_y - _oldY) * 1000 * 0.2 / cdt;
            _velA = cdt == 0 ? 0 : (a - _oldA)*1000 / cdt;

            // work with orientation
            _errorA = _destA - a;
            _oldA = a;

            _oldT = t;
            _oldX = p_x;
            _oldY = p_y;
            _estimated = false;
        }
        //System.out.println("X: "+_estX+"\tY: "+_estY);
       // System.out.println("error_x: " + _error_x + "\terror_y: " + _error_y + "\terror_a: " + _errorA);
        //System.out.println("VelX: "+_velX+"\tVelY: "+_velY+"\tVelA: "+_velA);

        // if the error is in the allowed error then there is no error
        // it is done to prevent a drift when one axis is there and another is not
        if (Math.abs(_error_x) <= Math.sqrt(_minXYError)) _error_x = 0;
        if (Math.abs(_error_y) <= Math.sqrt(_minXYError)) _error_y = 0;

        // acceleration towards destination point
        Double baseAccX = _error_x == 0 ? 0 : _maxAccX*_error_x/Math.abs(_error_x);
        Double baseAccY = _error_y == 0 ? 0 : _maxAccY*_error_y/Math.abs(_error_y);

        if (Math.sqrt(_error_x*_error_x + _error_y*_error_y) <= _nearDistance) {
            // gradually decrease acceleration if the drone is close to the destination
            baseAccX = _maxAccX*_error_x*1.0/_nearDistance;
            baseAccY = _maxAccY*_error_y*1.0/_nearDistance;
        }

        //System.out.println("baseAccX: "+baseAccX+"\tbaseAccY: "+baseAccY);

        // decrease acceleration if velocity is greater than the maximum
        Double velDecX = _maxAccX*(_velX*1.0/_maxVelX);
        Double velDecY = _maxAccY*(_velY*1.0/_maxVelY);

        //System.out.println("velDecX: "+velDecX+"\tvelDecY: "+velDecY);

        _accX = baseAccX - velDecX;
        _accY = baseAccY - velDecY;

        //System.out.println("AccX: "+_accX+"\tAccY: "+_accY);

        // calculate tilts: tilt gives an acceleration, the drone then drifts even when there is no tilt
        _frontBackTilt = Math.atan(_accY*1.0/9.8)/_maxPitch;
        _leftRightTilt = Math.atan(_accX*1.0/9.8)/_maxRoll;

        //System.out.println("roll: "+_leftRightTilt+"\tpitch: "+_frontBackTilt);

        // limit tilt to prevent rapid reaches
        if (Math.abs(_frontBackTilt) > 0.5) _frontBackTilt = 0.5*_frontBackTilt/Math.abs(_frontBackTilt);
        if (Math.abs(_leftRightTilt) > 0.5) _leftRightTilt = 0.5*_leftRightTilt/Math.abs(_leftRightTilt);



        _errorXY = Math.sqrt(_error_x*_error_x + _error_y*_error_y);

        //System.out.format("errorA: %.5f = %.5f - %.5f%n", _errorA, _destA, a);
        // if difference is greater than 360 (lower than -360 to the right), then it does not make sense to make a full rotation
        // and we cut the full circle.
        _errorA = _errorA < -360 ? _errorA%360 : _errorA > 360 ? _errorA%360 : _errorA;
        //System.out.format("errorA: %.5f%n", _errorA);
        // if difference is greater than 180 (lower than -180 to the right), then it does not make sense to make
        // a half-circle rotation, instead we just rotate to the opposite side.
        _errorA = _errorA < -180 ? _errorA%180 + 180 : _errorA > 180 ? _errorA%180 - 180 : _errorA;
        //System.out.format("errorA: %.5f%n", _errorA);

        _orientationSpeed = _errorA == 0 ? 0 : _maxOrientationSpeed*_errorA*1.0/Math.abs(_errorA);

        //System.out.println("roll: "+_leftRightTilt+"\tpitch: "+_frontBackTilt+"\tyaw: "+_orientationSpeed);

        //System.out.println("error_x: "+_error_x+"\terror_y: "+_error_y+"\terror_a: "+_errorA);
        //System.out.println("_errorXY: "+_errorXY);

        // should the distance to destination be within allowed error, we wipe out errors
        if (Math.abs(_errorXY) <= _minXYError) {
            _error_x = 0.0;
            _error_y = 0.0;
            _errorXY = 0.0;
            //System.out.println("target XY reached; extimated?: "+_estimated);
            // if the error is estimated then wait for a new calculated error
            if (_estimated) {
                // just hover
                _frontBackTilt = 0.0;
                _leftRightTilt = 0.0;
            }
        }

        // should the distance to destination be within allowed error, we wipe out errors
        if (Math.abs(_errorA) <= _minAError) {
            _errorA = 0.0;
            //System.out.println("target A reached; extimated?: "+_estimated);
            // if the error is estimated then wait for a new calculated error
            if (_estimated) _orientationSpeed = 0.0;
        }

        // if there is no error, or there are no new updates for at least 0.5 second then "freeze" one axis
        if (_error_x == 0 || t - _oldT > 500) _leftRightTilt = 0.0;
        if (_error_y == 0 || t - _oldT > 500) _frontBackTilt = 0.0;
        if (_errorA == 0 || t - _oldT > 500) {
            _velA = 0;
            _orientationSpeed = 0.0;
        }

        //System.out.println("roll: "+_leftRightTilt+"\tpitch: "+_frontBackTilt+"\tyaw: "+_orientationSpeed);


        //System.out.println("Arrived? "+(Math.abs(_errorXY) <= _minXYError && Math.abs(_errorA) <= _minAError && !_estimated));
        //System.out.println("------------------------------");
        //_frontBackTilt = 0.0;
    }

    @Override
    public boolean isArrived() {
        // drone is arrived when and only when the error within the minimum allowed error and it is not estimated
        return Math.abs(_errorXY) <= _minXYError && Math.abs(_errorA) <= _minAError && !_estimated;
    }

}
