package it.polimi.neslab.infrastructure.testBed.core;

public class PIDControl2 {
    //private double dState;
    private double iState;
    //private final double iMax;
    //private final double iMin;

    private final double iGain;
    private final double pGain;
    private final double dGain;

    private Double prevError = null;

    private long t = 0;

    public PIDControl2(/*double position, double iMax, double iMin,*/ double pGain,
                      double iGain, double dGain) {
        //super();
        //this.dState = position;
        this.iState = 0;
        //this.iMax = iMax;
        //this.iMin = iMin;
        this.iGain = iGain;
        this.pGain = pGain;
        this.dGain = dGain;
    }

    public void reset() {
        t = 0;
        prevError = null;
    }

    public double updatePID(double error, double dumb) {

        long dt = 0;
        if (t != 0) dt = System.currentTimeMillis() - t;
        t = System.currentTimeMillis();

        //double pTerm = updateP(error, dt);
        double iTerm = updateI(error, dt);
        double dTerm = updateD(error, dt);

        //System.out.println("pid:" + error + ", " + iTerm + ", " + dTerm);
        return pGain*error + iGain*iTerm + dGain*dTerm;
    }

    private double updateI(double error, long dt) {
        return iState += error*dt;
    }

    private double updateD(double error, long dt) {
        double dError = 0;
        if (prevError != null) dError = prevError - error;
        prevError = error;
        if (dt == 0 || dt > 1000 ||(dError == 0 && dt == 0)) return 0;
        return dError/dt;
    }

}