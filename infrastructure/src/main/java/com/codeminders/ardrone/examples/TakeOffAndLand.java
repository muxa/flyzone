package com.codeminders.ardrone.examples;

import it.polimi.neslab.infrastructure.testBed.Coordinate;

import com.codeminders.ardrone.ARDrone;
import com.codeminders.ardrone.NavData;
import com.codeminders.ardrone.NavDataListener;

public class TakeOffAndLand {

	private static Coordinate position = new Coordinate();

	private static final long CONNECT_TIMEOUT = 3000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ARDrone drone;

		try {
			// Create ARDrone object,
			// connect to drone and initialize it.
			drone = new ARDrone();
			drone.connect();
			drone.clearEmergencySignal();
			drone.trim();

			// Wait until drone is ready
			drone.waitForReady(CONNECT_TIMEOUT);

			drone.sendAllNavigationData();

			drone.addNavDataListener(new NavDataListener() {

				public void navDataReceived(NavData nd) {
					// TODO Auto-generated method stub
					//position.setZ(nd.getRawAltitude());
					System.out.println(nd.getRawAltitude());
				}
			});

			
			
			 Thread.sleep(3000);
			 
			 //ControlTower ct=new ControlTower(drone);
//			 VideoTest dr=new VideoTest(drone);
			 Thread.sleep(2000);
			 
//			 // drone.disconnect();
//			 drone.trim();
			  //Take off
			  System.err.println("Taking off");
			 drone.takeOff();
			
			 // Fly a little :)
			 drone.hover(6000);
			
			float altitude = 150;
			for(int i=0;i<6;i++){
			drone.move(0.03f, 0.0f, 0, 0, 500);
			}
			drone.hover(2000);
			
			for(int i=0;i<6;i++){
				drone.move(0f, -0.05f, 0, 0, 500);
				}
				drone.hover(2000);

			
//			
			
			// -------------------VAI IN SU------------------------

//			float minError = 0.8f;
//			float speedZ;
//			float speedOld = 0;
//
//			PIDControl pidZ = new PIDControl(position.getZ(), 10, 150, 0.008,
//					0.1, 0.03);
//			float error;
//			error = (float) (altitude - position.getZ());
//
//			while (Math.abs(error) > minError) {
//
//				speedZ = (float) pidZ.updatePID(error, position.getZ());
//
//				try {
//					if (speedZ > 1)
//						speedZ = 1;
//					if (Math.abs(speedZ) < Math.abs(speedOld))
//						speedZ = speedZ + 0.1f;
//
//					if (speedZ < 0)
//						speedZ = speedZ + 0.2f;
//
//					// System.out.println("Error:" + error + " speedZ:" +
//					// speedZ);
//
//					drone.move(0, 0, speedZ, 0, 100);
//					// System.out.println("sto correggendo...");
//					System.err.println(position.getZ());
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				error = (float) (altitude - position.getZ());
//			}
//
//			drone.hover(2000);

			// ------------------VAI IN GIU -----------------------

			// altitude=50;
			//
			//
			//
			// minError=5;
			//
			//
			//
			// error=(float) (altitude - position.getZ());
			//
			//
			// while(Math.abs(error)>minError){
			//
			// speedZ=(float) pidZ.updatePID(error, position.getZ());
			//
			// try {
			// drone.move(0, 0, speedZ, 0, 800);
			// Thread.sleep(200);
			// drone.hover();
			// System.out.println("sto correggendo...");
			// System.err.println("ERRORE:" + error);
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// error=(float) (altitude - position.getZ());
			// }
			//
			// drone.hover(2000);
			//

			//
			// drone.move(0.2f, -0.2f, -0.3f, -0.3f, 2000);

			// Thread.sleep(1000);

			// drone.hover(1000);

			// Thread.sleep(1000);

			// drone.turnRight(0.6f, 800);
			//
			//
			// drone.turnRight(0.4f, 800);
			//
			//
			// drone.turnRight(0.2f, 800);
			//
			//
			// drone.turnRight(0.1f, 800);

			/*
			 * drone.turnLeft(0.3f, 1200);
			 * 
			 * drone.hover(1000);
			 * 
			 * // move to the right for 1.2 sec drone.goRight(0.2f, 1200);
			 * 
			 * drone.hover(1000);
			 * 
			 * // move forward for 1.2 sec drone.goForward(0.2f, 1200);
			 * 
			 * drone.hover(1000);
			 * 
			 * // move left for 1.2 sec drone.goLeft(0.2f, 1200);
			 * 
			 * drone.hover(1000);
			 * 
			 * // move backward for 1.2 sec drone.goBackward(0.2f, 1200);
			 */
//			 drone.hover(2000);
			
			 
		
			 
			 //Land
//			 System.err.println("Landing");
//			 drone.move(0,0,1,0,1000);
//			 drone.hover(1000);
//			 drone.move(0, 0, 0.8f, 0, 3000);
			 drone.hover();
			 drone.land();
			 // Give it some time to land
			 Thread.sleep(2000);
			// Disconnect from the done
			 drone.disconnect();
			 System.out.println("DONE :)");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

}
